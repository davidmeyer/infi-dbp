package model;

import static org.junit.Assume.assumeNoException;
import static org.junit.jupiter.api.Assertions.*;

import java.security.Timestamp;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class DBManagerTest {

	static Person testPerson;
	static Notebook testNotebook;
	static Note testNote;

	@BeforeAll
	static void beforeAll() {
		createPerson();
		createNotebook();
		createNote();
	}
	
	@Test
	/** Connect to database */
	void establishConnection() {

		try {
			DBManager.getConnection();
		}
		catch(Exception e) {
			e.printStackTrace();
			fail("Couldn't connect to database");
		}
	}

	@Test
	/** Get all registered persons from database as Person Objects */
	void getPersons() {
		try {
			ArrayList<Person> persons = DBManager.loadPerson();
			if(persons == null || persons.size() < 1) {
				fail("No persons where found in the database");
			}
			assertEquals("Dave", persons.get(0).getFirstName());
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Get one specific person from database (via personId) */
	void getPersonViaId() {
		try {
			Person p = DBManager.loadPerson(1);
			if(p == null) {
				fail("Person was not found in the database");
			}
			else {
				System.out.println(p);
			}
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Get one specific person from database (via incomplete person object) */
	void getPersonViaProperties() {
		try {
			Person p = new Person();
			p.setEmail("test@gmail.com");
			p = DBManager.loadPerson(p).get(0);
			if(p == null) {
				fail("Person was not found in the database");
			}
			else {
				System.out.println(p);
			}
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			fail("IndexOutOfBoundsException: "+e);
			e.printStackTrace();
		}
	}

	@BeforeAll
	/** Create a test person account */
	static void createPerson() {
		System.out.println("Creating Test person");
		System.out.println("Creating a test person");
		Calendar cal = Calendar.getInstance();
		cal.set(1998, 8, 26);
		// TODO email unique
		testPerson = new Person("test@gmail.com", "pass", "Herzog", "Dave", "Meyer", cal.getTime());
		try {
			testPerson = DBManager.createPerson(testPerson);
			System.out.println("PersonId of the new test person: " + testPerson.getPersonId());
			testPerson.setPassword("pass");
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Login to a test person account */
	void loginPerson() {
		try {
			DBManager.loginPerson(testPerson.getEmail(), testPerson.getPassword());
			assertTrue(testPerson.equals(Person.getLoggedInPerson()));
//			assertEquals(testPerson, Person.getLoggedInPerson()); 
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
		catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Failed Login to a test person account (wrong password-email combination) */
	void failLoginPerson() {
		try {
			assertNull(DBManager.loginPerson(testPerson.getEmail(), "wrongPassword"));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
		catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}
	
	@Test
	void isPersonRegistered() {
		try {
			assertTrue(DBManager.isPersonRegistered(testPerson.getEmail()));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
		catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}
	
	@Test
	void isPersonNotRegistered() {
		try {
			assertFalse(DBManager.isPersonRegistered("thisemaildoesnotexist"));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
		catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Change properties of a test person account.
	 * A entry in the table 'notechange' should also be made */
	void changePerson() {
		fail("Not yet implemented");
	}

	@Test
	/** Delete a test person account */
	void deletePerson() {
		try {
			DBManager.deletePerson(11);
			assertNull(DBManager.loadPerson(11));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	/** Let the test user create a test notebook */
	static void createNotebook() {
		System.out.println("Creating Test notebook");
		testNotebook = new Notebook(testPerson, "My Notes");
		try {
			assertNotNull(testNotebook = DBManager.createNotebook(testNotebook));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
	}

	/** Let the test person create a test note inside the test notebook */
	static void createNote() {
		System.out.println("Creating Test note");
		testNote = new Note(testNotebook, "Grocery list: *caffeine\n*more caffeine\n", "My first note");
		try {
			testNote = DBManager.createNote(testNote);
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}

		//fail("Not yet implemented");
	}

	@Test
	void updateNote() {
//		System.out.println("test note");
		testNote.setContentText("new content text");
		try {
			DBManager.updateNote(testNote);
			Note n = DBManager.getNote(testNote.getId());
			assertEquals("new content text", n.getContentText());
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		} catch(NullPointerException e) {
			fail("NullPointer: "+e);
			e.printStackTrace();
		}
	}
	
	@Test
	void getNote() {
		try {
			Note note = DBManager.getNote(1);
			assertNotNull(note.getContentText());
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		} catch(NullPointerException e) {
			fail("NullPointer: "+e);
			e.printStackTrace();
		}
	}
	
	@Test
	/** Show all Notebooks a test person has made */
	void listNotebooksPerPerson() {
		try {
			ArrayList<Notebook> notebooks = DBManager.listNotebooksPerPerson(testPerson.getPersonId());
			notebooks.get(0).getAuthor().equals(testPerson);
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Show all Notes inside a test notebook */
	void listNotesPerNotebook() {
		try {
			ArrayList<Note> notes = DBManager.getNotes(testNotebook.getNotebookId());
			assertTrue(notes.get(0).equals(testNote));
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		}
	}

	@Test
	/** Create a test tag  */
	void createTag() {
		try {
			Tag tag = DBManager.createTag("testTag");
			assertEquals("testTag", tag.getTag());
		} catch (ClassNotFoundException e) {
			fail("ClassNotFoundException: "+e);
			e.printStackTrace();
		} catch (NullPointerException e) {
			fail("NullPointerException: "+e);
			e.printStackTrace();
		} catch (SQLException e) {
			fail("SQLException: "+e);
			e.printStackTrace();
		}	
	}

	@Test
	/** Add the test tag to the test note */
	void tagNote() {
		fail("Not yet implemented");
	}

	@Test
	/** Add the test tag to the test notebook */
	void tagNotebook() {
		fail("Not yet implemented");
	}

	@Test
	/** List the content (notebooks and notes) that are tagged by the test tag */
	void getContentByTag() {
		
		fail("Not yet implemented");
	}

	@Test
	/** Create a test group which persons can join */
	void createPersonGroup() {
		fail("Not yet implemented");
	}

	@Test
	/** Let the test user join the test group */
	void joinGroup() {
		fail("Not yet implemented");
	}

	@Test
	/** Grant the rights to see and edit the test note for persons inside the test group */ 
	void connectGroupToNote() {
		fail("Not yet implemented");
	}

	@Test
	/** Grant the rights to see and edit the test notebook for persons inside the test group */ 
	void connectGroupToNotebook() {
		fail("Not yet implemented");
	}

	@Test
	/** Deny the access to a note from persons who have no authorization */
	void unauthorizedAccessToNoteDenied() {
		fail("Not yet implemented");
	}

	@Test
	/** Deny the access to a notebook from persons who have no authorization */
	void unauthorizedAccessToNotebookDenied() {
		fail("Not yet implemented");
	}

	@Test
	/** Deny the editing of a note from persons who have no authorization */
	void unauthorizedEditingOfNoteDenied() {
		fail("Not yet implemented");
	}

	@Test
	/** Deny the editing of a notebook from persons who have no authorization */
	void unauthorizedEditingOfNotebookDenied() {
		fail("Not yet implemented");
	}
}