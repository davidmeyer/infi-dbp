package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PersonTest {

	@Test
	void isEmailAddressValidCheck() {
		assertTrue(Person.isEmail("david.meyer@gmail.com"));
	}

	@Test
	void isEmailAddressInvalidCheck() {
		assertFalse(Person.isEmail("david.meyer.com"));
		assertFalse(Person.isEmail("david.meyer@gmail"));
	}
}