package model;

/**
 * Describes the permissions a group on
 * notes or notebooks (Classes implementing INote Interface)
 *
 */
public class NoteXpersonGroup {

	private PersonGroup personGroup;
	/** Note or Notebook */
	private INote content;
	private boolean canSee;
	private boolean canEdit;
	
	public NoteXpersonGroup() {
		setCanSee(false);
		setCanEdit(false);
	}
	
	public NoteXpersonGroup(PersonGroup personGroup, INote content, boolean canSee, boolean canEdit) {
		this();
		setPersonGroup(personGroup);
		setContent(content);
		setCanSee(canSee);
		setCanEdit(canEdit);
	}

	public PersonGroup getPersonGroup() {
		return personGroup;
	}
	public void setPersonGroup(PersonGroup personGroup) {
		this.personGroup = personGroup;
	}
	public INote getContent() {
		return content;
	}
	public void setContent(INote content) {
		this.content = content;
	}
	public boolean isCanSee() {
		return canSee;
	}
	public void setCanSee(boolean canSee) {
		this.canSee = canSee;
	}
	public boolean isCanEdit() {
		return canEdit;
	}
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	public boolean equals(NoteXpersonGroup obj) {
		return this.getContent().equals(obj.getContent()) && this.getPersonGroup().equals(obj.getPersonGroup());
	}
	@Override
	public String toString() {
		return "NoteXpersonGroup[PersonGroupID:"+getPersonGroup().getPersonGroupId()+";"+
			((getContent() instanceof Note)?"NoteID:"+((Note) getContent()).getNoteId()  :"NotebookID"+((Notebook) getContent()).getNotebookId() )+"]";
	}
}