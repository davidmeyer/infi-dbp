package model;

import java.security.Timestamp;

public class NoteChange {

	private Note note;
	private Person person;
	private Timestamp pointInTime;
	/** First change of a note is the creation of it */
	private boolean isFirst;

	public NoteChange() {
		
	}
	
	public NoteChange(Note note, Person person, Timestamp pointInTime) {
		setNote(note);
		setPerson(person);
		setPointInTime(pointInTime);
	}
	
	public Note getNote() {
		return note;
	}
	public void setNote(Note note) {
		this.note = note;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Timestamp getPointInTime() {
		return pointInTime;
	}
	public void setPointInTime(Timestamp pointInTime) {
		this.pointInTime = pointInTime;
	}
	public boolean isFirst() {
		return isFirst;
	}
	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}
	
	public boolean equals(NoteChange obj) {
		// TODO check if timestamp comparison is valid
		return this.getNote().equals(obj.getNote()) && this.getPerson().equals(obj.getPerson()) && (this.getPointInTime().getTimestamp().getTime()==obj.getPointInTime().getTimestamp().getTime());
	}
	
	@Override
	public String toString() {
		return "NoteChange[NoteId:"+getNote().getNoteId()+";NoteContent:"+getNote().getContentText().substring(0, 10)
				+"...;PersonId:"+getPerson().getPersonId()+";Person:"+getPerson().getName()+"]";
	}
}