package model;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

public class Note implements INote {

	private int noteId;
	private Notebook notebook;
	private String contentText;
	private String title;
	private ArrayList<NoteChange> noteChanges;
	private ArrayList<Tag> noteTags; 
	private ArrayList<NoteXpersonGroup> permissions;
	
	private boolean shown;
	
	public Note() {
		noteChanges = new ArrayList<NoteChange>();
		noteTags = new ArrayList<Tag>();
		permissions = new ArrayList<NoteXpersonGroup>();
	}

	public Note(int noteId, Notebook notebook, String contentText, String title) {
		this(notebook, contentText, title);
		setNoteId(noteId);
	}
	
	public Note(int noteId, Notebook notebook, String contentText, String title, boolean shown) {
		this(noteId, notebook, contentText, title);
		setShown(shown);
	}
	
	public Note(Notebook notebook, String contentText, String title) {
		this();
		setNotebook(notebook);
		setContentText(contentText);
		setTitle(title);
	}
	
	public int getNoteId() {
		return noteId;
	}
	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}
	public Notebook getNotebook() {
		return notebook;
	}
	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
	public String getContentText() {
		return contentText;
	}
	public void setContentText(String contentText) {
		this.contentText = contentText;
	}
	public String getContentTextPreview() {
		if(getContentText() != null) {
			if(getContentText().length() > 13) {
				return getContentText().substring(0, 10)+"...";
			}
			return getContentText();
		}
		return "";
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public ArrayList<NoteChange> getNoteChanges() {
		return noteChanges;
	}
	public void setNoteChanges(ArrayList<NoteChange> noteChanges) {
		this.noteChanges = noteChanges;
	}
	public void addNoteChange(NoteChange noteChange) {
		this.noteChanges.add(noteChange);
	}
	public Person getAuthor() {
		for(int i = 0; i < getNoteChanges().size(); i++) {
			if(this.noteChanges.get(i).isFirst()) return noteChanges.get(i).getPerson();
		}
		return null;
	}
	public String getAuthorName() {
		if(getAuthor() != null) return getAuthor().getName();
		return "Unknown author";
	}
	public ArrayList<Tag> getNoteTags() {
		return noteTags;
	}
	public void setNoteTags(ArrayList<Tag> noteTag) {
		this.noteTags = noteTag;
	}
	public void addNoteTag(Tag tag)  {
		this.getNoteTags().add(tag);
	}
	public ArrayList<NoteXpersonGroup> getPermissions() {
		return permissions;
	}
	public void setPermissions(ArrayList<NoteXpersonGroup> permissions) {
		this.permissions = permissions;
	}
	public boolean isShown() {
		return shown;
	}
	public void setShown(boolean shown) {
		this.shown = shown;
	}
	public String getCreationDate() {
		if(getNoteChanges().size() > 0) {
			if(getNoteChanges().get(0).getPointInTime() != null) {
				return getNoteChanges().get(0).getPointInTime().getTimestamp().toString();
			}
		}
		return "Unknown creation date";
	}

	public ArrayList<SimpleEntry> getKeyValuePairs() {
		
		ArrayList<SimpleEntry> keyValuePairs = new ArrayList<SimpleEntry>();
		if(getNoteId() > 0) {
			keyValuePairs.add(new SimpleEntry<String, Integer>("noteId", getNoteId()));
		}
		if((getNotebook() != null) && (getNotebook().getNotebookId() > 0)) {
			keyValuePairs.add(new SimpleEntry<String, Integer>("notebook", getNotebook().getNotebookId()));
		}
		if((getContentText() != null) && (!getContentText().isEmpty())) {
			keyValuePairs.add(new SimpleEntry<String, String>("contentText", getContentText()));
		}
		if((getTitle() != null) && (!getTitle().isEmpty())) {
			keyValuePairs.add(new SimpleEntry<String, String>("title", getTitle()));
		}
		return keyValuePairs;
	}
	
	public boolean equals(Note obj) {
		return this.getNoteId() == obj.getNoteId();
	}
	
	@Override
	public String toString() {
		return "Note[ID:"+getNoteId()+";"+(getTitle()!=null?("Title:"+getTitle()+";"):"")+"Author:"+getAuthorName()
				+";Notebook:"+getNotebook().getNotebookId()+";Created:"+getCreationDate()
				+";"+getContentTextPreview()+"]";
	}

	@Override
	public int getId() {
		return this.getNoteId();
	}
}