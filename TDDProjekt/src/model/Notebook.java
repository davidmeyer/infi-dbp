package model;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;

public class Notebook implements INote {

	private int notebookId;
	private Person author;
	private String title;
	private Timestamp creationDate;
	
	private ArrayList<Note> notes;
	private ArrayList<Tag> notebookTags;
	
	public Notebook() {
		notes = new ArrayList<Note>();
		notebookTags = new ArrayList<Tag>();
	}
	
	public Notebook(Person author, String title) {
		this();
		setAuthor(author);
		setTitle(title);
	}
	
	public Notebook(int notebookId, Person author, String title, Timestamp creationDate) {
		this(author, title);
		setNotebookId(notebookId);
		setCreationDate(creationDate);
	}
	
	public Notebook(int notebookId, Person author, String title, long creationDate) {
		this(notebookId, author, title, new Timestamp(creationDate));
	}
	
	public int getNotebookId() {
		return notebookId;
	}
	public void setNotebookId(int notebookId) {
		this.notebookId = notebookId;
	}
	public Person getAuthor() {
		return author;
	}
	public void setAuthor(Person author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public ArrayList<Note> getNotes() {
		return notes;
	}
	public void setNotes(ArrayList<Note> notes) {
		this.notes = notes;
	}
	public void addNote(Note note) {
		getNotes().add(note);
	}
	public ArrayList<Tag> getNotebookTags() {
		return notebookTags;
	}
	public void setNotebookTags(ArrayList<Tag> notebookTags) {
		this.notebookTags = notebookTags;
	}
	public void addNotebookTag(Tag tag) {
		getNotebookTags().add(tag);
	}
	
	public ArrayList<SimpleEntry> getKeyValuePairs() {
		
		ArrayList<SimpleEntry> keyValuePairs = new ArrayList<SimpleEntry>();
		if(getNotebookId() > 0) {
			keyValuePairs.add(new SimpleEntry<String, Integer>("notebookId", getNotebookId()));
		}
		if(getAuthor() != null && getAuthor().getPersonId() > 0) {
			keyValuePairs.add(new SimpleEntry<String, Integer>("author", getAuthor().getPersonId()));
		}
		if(getTitle() != null && !getTitle().isEmpty()) {
			keyValuePairs.add(new SimpleEntry<String, String>("title", getTitle()));
		}
		if(getCreationDate() != null) {
			keyValuePairs.add(new SimpleEntry<String, Timestamp>("creationDate", getCreationDate()));
		}
		return keyValuePairs;
	}
	
	public boolean equals(Notebook obj) {
		return this.getNotebookId() == obj.getNotebookId();
	}
	@Override
	public String toString() {
		return "Notebook[ID:"+getNotebookId()+";"+(getTitle()!=null?("Title:"+getTitle()+";"):"")+"Author:"+getAuthor()
		+";Created:"+getCreationDate().toString()+";NumberOfNotes:"+getNotes().size()+"]";
	}

	@Override
	public int getId() {
		return getNotebookId();
	}
}