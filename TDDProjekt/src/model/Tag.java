package model;

public class Tag {

	private int tagId;
	private String tag;

	public Tag() {
		
	}
	
	public Tag(int tagId, String tag, INote content) {
		this(tagId, tag);
	}
	
	public Tag(int tagId, String tag) {
		setTagId(tagId);
		setTag(tag);
	}
	
	public int getTagId() {
		return tagId;
	}
	public void setTagId(int tagId) {
		this.tagId = tagId;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public boolean equals(Tag obj) {
		return this.getTagId() == obj.getTagId();
	}
	@Override
	public String toString() {
		return "Tag[TagID:"+getTagId()+";Tag:"+getTag()+"]";
	}
}