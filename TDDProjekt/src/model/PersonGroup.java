package model;

import java.util.ArrayList;

public class PersonGroup {

	private int personGroupId;
	private String title;
	private ArrayList<Person> persons;
	private ArrayList<NoteXpersonGroup> permissions;
	
	public PersonGroup() {
		persons = new ArrayList<Person>();
		permissions = new ArrayList<NoteXpersonGroup>();
	}
	
	public PersonGroup(int personGroupId, String title) {
		this();
		setPersonGroupId(personGroupId);
		setTitle(title);
	}
	
	public int getPersonGroupId() {
		return personGroupId;
	}
	public void setPersonGroupId(int personGroupId) {
		this.personGroupId = personGroupId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public ArrayList<Person> getPersons() {
		return persons;
	}
	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}
	public void addPerson(Person person) {
		getPersons().add(person);
	}
	public ArrayList<NoteXpersonGroup> getPermissions() {
		return permissions;
	}
	public void setPermissions(ArrayList<NoteXpersonGroup> permissions) {
		this.permissions = permissions;
	}
	
	public boolean equals(PersonGroup obj) {
		return this.getPersonGroupId()==obj.getPersonGroupId();
	}
	@Override
	public String toString() {
		return "PersonGroup[GroupID:"+getPersonGroupId()+";"+(getTitle()!=null?("Title:"+getTitle()+";"):"")+"NumberOfMembers:"+getPersons().size()+"]";
	}
}