package model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import control.Settings;

public class Person {

	private int personId = -1;
	private String email;
	private String password;
	private String title;
	private String firstName;
	private String lastName;
	private Date birthday;
	private Timestamp creationDate;
	private ArrayList<PersonGroup> groups;
	private ArrayList<NoteChange> noteChanges;
	private ArrayList<Notebook> notebooks;

	private static Person loggedInPerson;
	
	// CONSTRUCTORS
	
	public Person() {
		groups = new ArrayList<PersonGroup>();
		noteChanges = new ArrayList<NoteChange>();
		notebooks = new ArrayList<Notebook>();
	}
	
	public Person(int personId) {
		this();
		setPersonId(personId);
	}
	
	public Person(String email, String password, String title, String firstName, String lastName, Date birthday) {
		this();
		setEmail(email);
		setPassword(password);
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setBirthday(birthday);
	}
	
	public Person(int personId, String email, String password, String title, String firstName, String lastName, Date birthday, Timestamp creationDate) {
		this(email, password, title, firstName, lastName, birthday);
		setPersonId(personId);
		setCreationDate(creationDate);
	}
	
	public Person(int personId, String email, String title, String firstName, String lastName, long birthday, long creationDate) {
		this();
		setPersonId(personId);
		setEmail(email);
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setBirthday(new Date(birthday));
		setCreationDate(new Timestamp(creationDate));
	}
	
	public Person(String email, String password) {
		this();
		setEmail(email);
		setPassword(password);
		setFirstName("1");
		setLastName("2");
		setTitle("3");
		setBirthday(new Date());
	}
	
	// STATIC FUNCTIONS
	/** Is the given String a valid email address?
	 */
	public static boolean isEmail(String email) {
		
		String[] splits = email.split("@");
		if(splits.length != 2) return false; // No @ Symbol or more than 1 @ symbol
		if(!splits[1].contains(".")) return false; // No domain name (.com, .at, .net, etc.)
		// TODO more checks
		return true;
	}
	
	// GETTER & SETTER
	
	public static Person getLoggedInPerson() {
		return loggedInPerson;
	}

	/**
	 * For logout, set loggedInPerson parameter null 
	 * @param loggedInPerson
	 */
	public static void setLoggedInPerson(Person loggedInPerson) {
		Person.loggedInPerson = loggedInPerson;
		if(Person.loggedInPerson != null) { // Login
			Settings.getInstance().setPersonId(Person.loggedInPerson.getPersonId());
			Settings.saveSettings();
			System.out.println("Person "+Person.loggedInPerson+" logged in");
		}
		else { // Logout
			Settings.getInstance().setPersonId(-1);
			Settings.saveSettings();
			System.out.println("Person logged out");
		}
	}

	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/** first name and last name */
	public String getName() {
		return getFirstName() + " " + getLastName();
	}
	/** Title, first name and last name */
	public String getFullName() {
		return getTitle()+" "+getName();
	}
	/** Returns the birthdate of this person in a format
	 * suitable for database insertions (yyyy-MM-dd)
	 * @return Birthday/Birthdate of this person (as String)
	 */
	public String getBirthdayAsString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(birthday);
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	private void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public ArrayList<PersonGroup> getGroups() {
		return groups;
	}
	public void setGroups(ArrayList<PersonGroup> groups) {
		this.groups = groups;
	}
	public void addGroup(PersonGroup group) {
		this.groups.add(group);
	}
	public ArrayList<NoteChange> getNoteChanges() {
		return noteChanges;
	}
	public void setNoteChanges(ArrayList<NoteChange> noteChanges) {
		this.noteChanges = noteChanges;
	}
	public void addNoteChange(NoteChange noteChange) {
		this.noteChanges.add(noteChange);
	}
	public ArrayList<Note> getCreatedNotes() {
		ArrayList<Note> notes = new ArrayList<Note>();
		for(int i = 0; i < getNoteChanges().size(); i++) {
			if(this.noteChanges.get(i).isFirst()) notes.add(noteChanges.get(i).getNote());
		}
		return notes;
	}
	public ArrayList<Notebook> getNotebooks() {
		return notebooks;
	}
	public void setNotebooks(ArrayList<Notebook> notebooks) {
		this.notebooks = notebooks;
	}
	public void addNotebook(Notebook notebook) {
		this.notebooks.add(notebook);
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<SimpleEntry> getKeyValuePairs() {
		
		ArrayList<SimpleEntry> keyValuePairs = new ArrayList<SimpleEntry>();
		
		if(this.getEmail() != null) {
			// Sample:
			keyValuePairs.add(new SimpleEntry<String, String>("email", this.getEmail()));
		}
		else {
			throw new NullPointerException("The E-Mail address of a new person was null, which is not permitted ("+this+")");
		}
		if(this.getPassword() != null) {
			keyValuePairs.add(new SimpleEntry<String, String>("password", this.getPassword()));
		}
		/*else {
			throw new NullPointerException("The password of a new person was null, which is not permitted ("+this+")");
		}*/
		if(this.getTitle() != null) {
			keyValuePairs.add(new SimpleEntry<String, String>("title", this.getTitle()));
		}
		if(this.getFirstName() != null) {
			keyValuePairs.add(new SimpleEntry<String, String>("firstName", this.getFirstName()));
		}
		if(this.getLastName() != null) {
			keyValuePairs.add(new SimpleEntry<String, String>("lastName", this.getLastName()));
		}
		if(this.getBirthday() != null) {
			keyValuePairs.add(new SimpleEntry<String, String>("birthday", this.getBirthdayAsString()));
		}
		/*if(this.getCreationDate() != null) {
			keyValuePairs.add(new SimpleEntry<String, Long>("creationDate", this.getCreationDate().getTime()/1000));
		}*/ // TODO
		return keyValuePairs;
	}
	
	public boolean equals(Person obj) {
		return this.getPersonId() == obj.getPersonId();
	}
}