package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

public abstract class DBManager {

	private static final String HOST = "localhost";
	private static final String USER = "postgres";
	private static final String PASSWORD = "passwort";
	private static final String DATABASE = "TDDProjekt";

	static Connection con;

	public static Connection getConnection() throws SQLException, ClassNotFoundException {

		if(con == null) {
			System.out.println("Trying to connect to database...");
			Class.forName("org.postgresql.Driver");
			con =  DriverManager.getConnection("jdbc:postgresql://"+HOST+"/"+DATABASE+"?user="+USER+"&password="+PASSWORD);
			System.out.println("Successfully connected to PostgreSQL database!");
		}
		return con;
	}	

	// === NOTES ===

	/** Get all notes a person has created or edited 
	 * @param person Person whose notes should be loaded
	 * @param author true = Person needs to be the author/creator of the note ;
	 * false = Person needs to just have edited the note at some point
	 */
	public ArrayList<Note> getNotes(Person person, boolean author) {
		return null;
	}

	/**
	 * 
	 * @param notebook
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public ArrayList<Note> getNotes(Notebook notebook) throws ClassNotFoundException, SQLException {
		PreparedStatement stm = getConnection().prepareStatement("SELECT noteId, notebook, contentText, title FROM note WHERE notebook = ?");
		stm.setInt(1, notebook.getNotebookId());
		ArrayList<Note> notes = new ArrayList<Note>();
		ResultSet result = stm.executeQuery();
		while(result.next()) {

			notes.add(new Note(result.getInt(1), notebook, result.getString(3), result.getString(4)));
		}
		return notes;
	}

	public static ArrayList<Note> getNotes(int notebookId) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT noteId, notebook, contentText, title FROM note WHERE notebook = ?");
		stm.setInt(1, notebookId);

		Notebook notebook = loadNotebook(notebookId);

		ArrayList<Note> notes = new ArrayList<Note>();
		ResultSet result = stm.executeQuery();
		while(result.next()) {

			notes.add(new Note(result.getInt(1), notebook, result.getString(3), result.getString(4)));
		}
		return notes;
	}

	public ArrayList<Note> getNotes(Tag tag) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT noteId, notebook, contentText, title FROM note n"
				+ " JOIN noteTag nt ON(n.noteId=nt.contentId) WHERE nt.isNote = true AND nt.tag = ?");
		stm.setInt(1, tag.getTagId());

		ArrayList<Note> notes = new ArrayList<Note>();
		ResultSet result = stm.executeQuery();
		Notebook notebook;
		while(result.next()) {

			notebook = loadNotebook(result.getInt(2));
			notes.add(new Note(result.getInt(1), notebook, result.getString(3), result.getString(4)));
		}
		return notes;
	}

	public ArrayList<Note> getNotes(PersonGroup group) {
		return null;
	}

	public static Note createNote(Note note) throws ClassNotFoundException, SQLException {

		String query1 = "INSERT INTO note(";
		String query2 = ") VALUES (";

		ArrayList<SimpleEntry> keyValuePairs = note.getKeyValuePairs();
		for(int i = 0; i < keyValuePairs.size(); i++) {
			if(i > 0) {
				query1 += ", ";
				query2 += ", ";
			}
			query1 += keyValuePairs.get(i).getKey();
			query2 += (keyValuePairs.get(i).getValue() instanceof String?"'":"") 
					+keyValuePairs.get(i).getValue()
					+(keyValuePairs.get(i).getValue() instanceof String?"'":"");
		}
		Statement stm = getConnection().createStatement();
		stm.execute(query1+query2+")");
		
//		PreparedStatement pstm = getConnection().prepareStatement("SELECT noteId, notebook, contenttext, title FROM note WHERE notebook = ? AND contenttext = ? AND title = ? ORDER BY noteId DESC");
		PreparedStatement pstm = getConnection().prepareStatement("SELECT * FROM note ORDER BY noteId DESC");
		/*if(note.getNotebook() == null) pstm.setInt(1, 1); // TODO
		pstm.setInt(1, note.getNotebook().getId());
		pstm.setString(2, note.getContentText());
		pstm.setString(3, note.getTitle());*/
		ResultSet result = pstm.executeQuery();
		while(result.next()) {
			return new Note(result.getInt(1), loadNotebook(result.getInt(2)), result.getString(3), result.getString(4));
		}
		return null;
	}
	
	public static void updateNote(Note note) throws ClassNotFoundException, SQLException {
		
		PreparedStatement pstm = getConnection().prepareStatement("UPDATE note SET contenttext = ?, title = ? WHERE noteId = ?");
		pstm.setString(1, note.getContentText());
		pstm.setString(2, note.getTitle());
		pstm.setInt(3, note.getId());
		pstm.execute();
		
		if(note.getAuthor() != null) {
			PreparedStatement pstm2 = getConnection().prepareStatement("INSERT INTO notechange (note, person) VALUES (?, ?)");
			pstm2.setInt(1, note.getId());
			pstm2.setInt(2, note.getAuthor().getPersonId());
			pstm2.executeQuery();
		}
		
//		return getNote(note.getId());
		// TODO WIP implement
	}
	
	public static Note getNote(int noteId) throws ClassNotFoundException, SQLException {
		PreparedStatement pstm = getConnection().prepareStatement("SELECT noteId, notebook, contenttext, title FROM note WHERE noteId = ?");
		pstm.setInt(1, noteId);
		ResultSet result = pstm.executeQuery();
		while(result.next()) {
			return new Note(result.getInt(1), loadNotebook(result.getInt(2)), result.getString(3), result.getString(4));
		}
		return null;
	}

	// === NOTEBOOK ===

	public static Notebook loadNotebook(int notebookId) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT author, title, creationDate FROM notebook WHERE notebookId = ?");
		stm.setInt(1, notebookId);
		ResultSet result = stm.executeQuery();
		result.next();
		return new Notebook(notebookId, loadPerson(result.getInt(1)), result.getString(2), result.getTimestamp(3));
	}

	/** Loads a specific notebook and all notes within them   
	 * @throws SQLException 
	 * @throws ClassNotFoundException */
	public Notebook loadNotebookWithNotes(int notebookId) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT nb.author, nb.title, nb.creationDate FROM notebook nb"
				+ " JOIN note n ON(nb.notebookId=n.notebook)");

		ResultSet result = stm.executeQuery();
		result.next();
		Notebook notebook = new Notebook(notebookId, loadPerson(result.getInt(1)), result.getString(2), result.getTimestamp(3).getTime()/1000);
		notebook.setNotes(getNotes(notebookId));
		return notebook;
	}

	public static ArrayList<Notebook> loadNotebooksWithNotes(Person p) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT notebookId, title, creationDate FROM notebook WHERE author = ?");
		stm.setInt(1, p.getPersonId());
		ArrayList<Notebook> notebooks = new ArrayList<Notebook>();
		ResultSet result = stm.executeQuery();
		while(result.next()) {

			notebooks.add(new Notebook(result.getInt(1), p, result.getString(2), result.getTimestamp(3)));
			notebooks.get(notebooks.size()-1).setNotes(getNotes(result.getInt(1)));
		}
		return notebooks;
	}

	public static ArrayList<Notebook> loadNotebooks(Notebook nb) throws ClassNotFoundException, SQLException {

		String query1 = "SELECT notebookId, author, title, creationDate FROM notebook WHERE ";
		ArrayList<SimpleEntry> keyValuePairs = nb.getKeyValuePairs();
		for(int i = 0; i < keyValuePairs.size(); i++) {
			if(i > 0) {
				query1 += " AND ";
			}
			query1 += keyValuePairs.get(i).getKey();
			query1 += "=";
			query1 += (keyValuePairs.get(i).getValue() instanceof String?"'":"") 
					+keyValuePairs.get(i).getValue()
					+(keyValuePairs.get(i).getValue() instanceof String?"'":"");
		}

		Statement stm = getConnection().createStatement();
		ResultSet result = stm.executeQuery(query1);
		ArrayList<Notebook> notebooks = new ArrayList<Notebook>();
		while(result.next()) {
			notebooks.add(new Notebook(result.getInt(1), loadPerson(result.getInt(2)), result.getString(3), result.getTimestamp(4)));
		}
		return notebooks;
	}

	public static Notebook createNotebook(Notebook nb) throws ClassNotFoundException, SQLException {

		ArrayList<SimpleEntry> keyValuePairs = nb.getKeyValuePairs();
		String query1 = "INSERT INTO notebook (";
		String query2 = ") VALUES (";

		for(int i = 0; i < keyValuePairs.size(); i++) {
			if(i > 0) {
				query1 += ", ";
				query2 += ", ";
			}
			System.out.println("keyvalue: " + keyValuePairs.get(i).getKey() + "-" + keyValuePairs.get(i).getValue());
			query1 += keyValuePairs.get(i).getKey();
			query2 += (keyValuePairs.get(i).getValue() instanceof String?"'":"")
					+keyValuePairs.get(i).getValue()
					+(keyValuePairs.get(i).getValue() instanceof String?"'":"");
		}
		Statement stm = getConnection().createStatement();
		System.out.println("createNotebook query" + query1+query2+")");
		stm.execute(query1+query2+")");
		return loadNotebooks(nb).get(0);
	}

	public static ArrayList<Notebook> listNotebooksPerPerson(int personId) throws ClassNotFoundException, SQLException  {
		PreparedStatement pstm = DBManager.getConnection().prepareStatement("SELECT notebookId, author, title, creationDate FROM notebook WHERE author = ?");// TODO preparen
		pstm.setInt(1, personId);
		ResultSet result = pstm.executeQuery();
		ArrayList<Notebook> notebooks = new ArrayList<Notebook>();
		while(result.next()) {
			// TODO persons cache
			notebooks.add(new Notebook(result.getInt(1), loadPerson(result.getInt(2)), result.getString(3), result.getTimestamp(4).getTime()));
		}
		return notebooks;
	}
	
	// === PERSON ===

	/**
	 * Create an account for a new person
	 * @param person The Person for whom the account should be made
	 * (object does not contain a personId or creationDate yet)
	 * @return Person object which was read from the database
	 * (now contains also personId and creationDate in addition to the other properties)
	 */
	public static Person createPerson(Person person) throws ClassNotFoundException, SQLException, NullPointerException  {

		Statement stm = getConnection().createStatement();
		String query1 = "INSERT INTO person(";
		String query2 = ") VALUES (";

		// Check if a password is given (other mandatory fields are checked in the keyValuePairs method later)
		if(person.getPassword() == null || person.getPassword().isEmpty()) {
			throw new NullPointerException("The password of a new person was null, which is not permitted ("+person+")");
		}

		ArrayList<SimpleEntry> keyValuePairs = person.getKeyValuePairs();
		for(int i = 0; i < keyValuePairs.size(); i++) {
			if(i > 0) {
				query1 += ", ";
				query2 += ", ";
			}
			query1 += keyValuePairs.get(i).getKey();
			query2 += ((keyValuePairs.get(i).getValue() instanceof String)?"'":"")
					+keyValuePairs.get(i).getValue()
					+((keyValuePairs.get(i).getValue() instanceof String)?"'":"");
		}
		query1 += query2 + ")";

		boolean ergb = stm.execute(query1);
		System.out.println(ergb?"Sucessfully inserted person into the database!":"An error occured while inserting a person into the database!");
		return loadPerson(person).get(0);
	}

	/** Uses the email address and password to login into an user account.
	 * If email-password combination is correct, a person object representing
	 * this user is returned and saved als currently-logged-in person (inside the person class)
	 * If the credentials are wrong, null is returned.
	 * 
	 * @param email E-Mail address with which the account was created
	 * @param password Password string of the person-account linked to the given email address (plaintext)
	 * @return person object representing the logged in person (or null if credentials were wrong)
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Person loginPerson(String email, String password) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("SELECT personId, email, title, firstName, lastName, birthday, creationDate FROM person WHERE email LIKE ? AND password LIKE ?");
		stm.setString(1, email);
		System.out.println(email + " - " + password);
		// TODO password encryption
		stm.setString(2, password);
		ResultSet result = stm.executeQuery();
		if(!result.next()) return null;
		Person p = new Person(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
				 (result.getDate(6)==null)?0:result.getDate(6).getTime(), (result.getDate(7)==null)?0:result.getDate(7).getTime());
		Person.setLoggedInPerson(p);
		return p;
	}
	
	/**
	 * @return true = Person is registered in database ; false = Person is not registered in database
	 */
	public static boolean isPersonRegistered(String email) throws ClassNotFoundException, SQLException {
		
		PreparedStatement stm = getConnection().prepareStatement("SELECT * FROM person WHERE email LIKE ?");
		stm.setString(1, email);
		ResultSet result = stm.executeQuery();
		if(result.next()) return true;
		return false;
	}

	/**
	 * Loads all registered persons
	 * @return ArrayList which contains all registered persons (saved via Person objects)
	 */
	public static ArrayList<Person> loadPerson() throws ClassNotFoundException, SQLException {

		Statement stm = getConnection().createStatement();
		String query = "SELECT personId, email, title, firstName, lastName, birthday, creationDate FROM person"; // TODO preparedStatements
		ResultSet result = stm.executeQuery(query);
		ArrayList<Person> persons = new ArrayList<Person>();
		while(result.next()) {

			persons.add(new Person(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
					(result.getDate(6)==null)?0:result.getDate(6).getTime(), (result.getDate(7)==null)?0:result.getDate(7).getTime()));
			System.out.println(persons.get(persons.size()-1));
		}
		return persons;
	}

	/**
	 * 
	 * @param personId
	 * @return Person with the given personId; null if the personId was not found in the database
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Person loadPerson(int personId) throws ClassNotFoundException, SQLException {

		String query = "SELECT personId, email, title, firstName, lastName, birthday, creationDate FROM person WHERE personId = ?";
		PreparedStatement stm = getConnection().prepareStatement(query); // TODO prepareStatement

		stm.setInt(1, personId);
		ResultSet result = stm.executeQuery();
		if(!result.next()) return null;
		Person person = new Person(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
				(result.getDate(6)==null)?0:result.getDate(6).getTime(), (result.getDate(7)==null)?0:result.getDate(7).getTime());

		return person;
	}

	public static ArrayList<Person> loadPerson(Person p) throws ClassNotFoundException, SQLException {

		Statement stm = getConnection().createStatement();
		String query = "SELECT personId, email, title, firstName, lastName, birthday, creationDate FROM person WHERE "; // TODO preparedStatements

		ArrayList<SimpleEntry> personProperties = p.getKeyValuePairs();
		for(int i = 0; i < personProperties.size(); i++) {
			if(i > 0) query += " AND ";
			query += personProperties.get(i).getKey()+" = "
					+(personProperties.get(i).getValue() instanceof String?"'":"")
					+personProperties.get(i).getValue()
					+(personProperties.get(i).getValue() instanceof String?"'":"");
		}

		ArrayList<Person> persons = new ArrayList<Person>();
		ResultSet result = stm.executeQuery(query);
		while(result.next()) {

//			persons.add(new Person(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
//					(result.getDate(6)==null)?0:result.getDate(6).getTime(), 0)); // TODO
			persons.add(new Person(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),
					(result.getDate(6)==null)?0:result.getDate(6).getTime(), (result.getDate(7)==null)?0:result.getDate(7).getTime()));
			System.out.println(persons.get(persons.size()-1));
		}
		return persons;
	}

	/** Loads the given person data with notebooks containing notes 
	 * @throws SQLException 
	 * @throws ClassNotFoundException */
	public static Person loadPersonWithContent(Person p) throws ClassNotFoundException, SQLException {

		p = loadPerson(p).get(0);
		p.setNotebooks(loadNotebooksWithNotes(p));
		return p;
	}

	public static void deletePerson(int personId) throws ClassNotFoundException, SQLException {

		PreparedStatement stm = getConnection().prepareStatement("DELETE FROM person WHERE personId = ?"); // TODO preparedStatement?
		stm.setInt(1, personId);
		stm.execute();
	}

	// === TAG ===
	
	public static Tag createTag(String tagName) throws ClassNotFoundException, SQLException, NullPointerException {
		
		PreparedStatement stm = getConnection().prepareStatement("INSERT INTO tag (tag) VALUES (?)");
		stm.setString(1, tagName);
		stm.execute();
		return getTags(tagName);
	}
	
	public static Tag getTags(String tagName) throws ClassNotFoundException, SQLException, NullPointerException {
		PreparedStatement stm = getConnection().prepareStatement("SELECT tagId, tag FROM tag WHERE tag = ?");
		stm.setString(1, tagName);
		ResultSet result = stm.executeQuery();
		result.next();
		Tag tag = new Tag(result.getInt(1), result.getString(2));		
		return tag;
	}
	
	public static ArrayList<Tag> getTags() throws ClassNotFoundException, SQLException {
		Statement stm = getConnection().createStatement();
		ResultSet result = stm.executeQuery("SELECT tagId, tag FROM tag");
		ArrayList<Tag> tags = new ArrayList<Tag>();
		while(result.next()) {
			tags.add(new Tag(result.getInt(1), result.getString(2)));
		}
		return tags;
	}
	
	public static void tagNote(Tag tag, INote content) throws ClassNotFoundException, SQLException {
		
		PreparedStatement stm = getConnection().prepareStatement("INSERT INTO notetag (contentId, tag, isNote) VALUES (?, ?, ?)");
		stm.setInt(1, content.getId());
		stm.setInt(2, tag.getTagId());
		stm.setBoolean(3, (content instanceof Note));
		stm.execute();
	}
	
	public static ArrayList<Note> getContentPerTag(Tag tag) throws SQLException, ClassNotFoundException {
		PreparedStatement ps = getConnection().prepareStatement("SELECT n.noteId, n.notebook, n.contenttext, n.title FROM note n JOIN notetag nt ON(n.noteId=nt.contentId) WHERE nt.isNote = true AND nt.tag = ?");
		ps.setInt(1, tag.getTagId());
		ResultSet result = ps.executeQuery();
		ArrayList<Note> notes = new ArrayList<Note>();
		while(result.next()) {
			notes.add(new Note(result.getInt(1), loadNotebook(result.getInt(2)), result.getString(3), result.getString(4)));
		}
		return notes;
	}
	

}