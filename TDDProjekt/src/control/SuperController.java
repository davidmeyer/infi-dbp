package control;

import java.io.IOException;
import java.nio.channels.NonWritableChannelException;

import control.enums.DragStatus;
import control.enums.EdgeMode;
import control.listener.ResizeEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Cursor;

/**
 * Controller Superklasse fuer andere Controller.
 * Controller dienen dazu, die GUI eines FXML Dokumentes
 * anzuzeigen und Interaktionen damit zu ermoeglichen
 * 
 * Nach der Aenderung der FXML Datei muss diese neu geladen werden
 * 
 * @author Dave
 */
public class SuperController {

	private static int startWidth = 100;
	private static int startHeight = 100;

	private static Color startColor = new Color(0.5, 0.5, 0.5, 0.4);

	/** Kann die Position durch klicken und ziehen verändert werden?
	 * (nur positionieren, nicht skalieren) */
	private boolean dragable = true;
	private boolean certainPaneDragable = true;
	private boolean certainPaneDragableNow = certainPaneDragable;
	private boolean resizeable = true;
	private boolean selectable = true;
	private boolean selected = false;
	
	private boolean freigabe = false;

	/** Startposition und Skalierung */
	protected Rectangle startRect;
	
	/** Der Dateiname bzw. der Pfad zur FXML Datei, dessen GUI der Controller verwaltet */
	private String pathToFXML;
	
	public static final String FXML_PATH = "/view/";
	
	private Node loadedNode;
	
	private Stage thisStage = null;
	
	public static final float resizePercentDefault = 0.05f;
	public static float resizePercent = resizePercentDefault;

	public static final int standardMinHeight = 10;
	public static final int standardMinWidth = 10;

	/** Sollen Debug-Nachrichten in die Konsole geschrieben werden? */
	private boolean debug = false;

	/** Status des Drags */
	public DragStatus dragStatus;
	/** Status des Hovers */
	public DragStatus hoverStatus;
	/** X-Abstand zwischen der Position des Mausklicks
	 * und dem (linken) Rand des DragablePanes
	 * (Wert nur sinnvoll w�hrend des Dragens) */
	double xDragStart;
	/** Y-Abstand zwischen der Position des Mausklicks
	 * und dem (oberen) Rand des DragablePanes
	 * (Wert nur sinnvoll w�hrend des Dragens) */
	double yDragStart;
	
	private SuperController() {
		
	}
	
	/**
	 * 
	 * @param fxmlName Name der FXML Datei (ohne .fxml, ohne Pfad)
	 */
	public SuperController(String fxmlName) {
		this();
		setFXMLName(fxmlName);
	}
	
	public SuperController sc;
	
	/** Erzwingt das Laden der FXML Datei
	 * (muss vom End-User nicht gemacht werden, da es bei Bedarf automatisch ausgefuehrt wird) */
	protected Node loadFXML() {
		if(this.pathToFXML != null) {
			FXMLLoader loader = new FXMLLoader();
			//System.out.println(Class.class.getResource(pathToFXML));
			loader.setLocation(Class.class.getResource(pathToFXML));
			try {
				
				loadedNode = loader.load();
				//loadedNode = loader.load(Class.class.getResource(pathToFXML).openStream());
				sc = loader.getController();
				sc.sc = this;
				// TODO check 4 other loading operations
			} catch (IOException e) {
				e.printStackTrace();
			}
			return loadedNode;
		}
		else { // Pfad zur FXML nicht vorhanden
			System.out.println("FEHLER: Konnte FXML Datei nicht laden, da der Pfad nicht angegeben wurde @ " + this);
		}
		return loadedNode;
	}
	
	/** Gibt die GUI als Node zurueck
	 *  Ladet sie, wenn sie nicht existiert
	 *  Deswegen muss FXML Path gegeben sein
	 */
	public Node getGUIAsNode() {
		if(loadedNode == null) loadFXML();
		return loadedNode;
	}
	
	public AnchorPane getGUIAsAnchorPane() {
		return new AnchorPane(getGUIAsNode());
	}
	
	public Scene getGUIAsScene() {
		
		return new Scene((Parent) getGUIAsNode());
	}
	
	/**
	 * Ladet die GUI in die angegebene Stage und gibt sie zurueck
	 * ACHTUNG: Die Scene der Stage wird geandert!
	 * @param stage Die Stage in der die GUI angezeigt werden soll
	 * @return die angegebene Stage, die jetzt di GUI enthaelt
	 * @throws NullPointerException Wenn FXML Datei nicht gesetzt wurde
	 * @throws IOException 
	 */
	public Stage getGUIAsStage(Stage stage) {
		stage.setScene(getGUIAsScene());
		thisStage = stage;
		thisStage.getIcons().add(new Image("/view/icon/FuturePDF.png"));
		return stage;
	}
	
	/**
	 * Erzeugt eine neue Stage, auf der die GUI angezeigt werden
	 * @return
	 * @throws NullPointerException
	 * @throws IOException
	 */
	public Stage getGUIAsStage() {
		return getGUIAsStage(true, true);
	}
	
	public Stage getGUIAsStage(boolean resizable, boolean showTitle) {
		Stage stage = new Stage();
		thisStage = stage;
		//thisStage.getIcons().add(new Image("/view/icon/FuturePDF.png"));
		if(!showTitle) stage.initStyle(StageStyle.TRANSPARENT);
		stage.setScene(getGUIAsScene());
		stage.show();
		stage.setResizable(resizable);
		stage.getIcons().add(new Image("/view/images/Notz_Logo_v1.png"));
		return stage;
	}
	
	public Stage getGUIAsStage(double width, double height) {
		Stage stage = getGUIAsStage();
		stage.setWidth(width);
		stage.setHeight(height);
		return stage;
	}
	
	public Stage getGUIAsStage(String title) {
		Stage stage = getGUIAsStage();
		stage.setTitle(title);
		return stage;
	}
	
	public Stage getGUIAsStage(String title, double width, double height) {
		Stage stage = getGUIAsStage();
		stage.setTitle(title);
		stage.setWidth(width);
		stage.setHeight(height);
		return stage;
	}
	
	// SETTER & GETTER
	/**
	 * Setzt den Pfad zur FXML Datei relativ zum Source Package mit .fxml
	 * Bsp: /view/fxml/ClosePopup.fxml
	 * @param pathToFXML Pfad zur FXML Datei
	 */
	public void setPathToFXML(String pathToFXML) {
		this.pathToFXML = pathToFXML;
		loadedNode = null; // Damit die FXML Datei neu geladen wird
//		loadFXML(); // TODO geht nit - warum?
		System.out.println("Der Pfad zur FXML Datei von " + this + " wurde auf");
		System.out.println(this.pathToFXML+"\ngeandert");
	}
	/**
	 * Setzt den Namen der FXML Datei
	 * Bsp: ClosePopup oder ClosePopup.fxml
	 * (mit oder ohne .fxml, wird automatisch hinzugefuegt falls noetig)
	 * Pfad darf nicht angegeben werden
	 * Es wird der Default Pfad unter FXML_PATH automatisch verwendet
	 * @param fxmlName Name der FXML (ohne .fxml, ohne Pfad)
	 */
	public void setFXMLName(String fxmlName) {
		String[] splits = fxmlName.split("."); // TODO
		if(splits.length < 1) { // kein .fxml -> .fxml dran h�ngen
			fxmlName += ".fxml";
		}
		this.setPathToFXML(FXML_PATH + fxmlName);
	}
	
	/**
	 * Bsp: /view/fxml/ClosePopup.fxml
	 * @return Gibt den Pfad zur FXML relativ zum Source Package zurueck
	 */
	
	public String getPathToFXML() {
		return pathToFXML;
	}
	/**
	 * Bsp: ClosePopup.fxml 
	 * @return Name der FXML Datei (ohne Pfad, mit '.fxml')
	 */
	public String getFXMLName() {
		String[] splits = pathToFXML.split("/"); // TODO Trennzeichen Variable?
		return splits[splits.length];
	}

	public Stage getThisStage() {
		return thisStage;
	}

	public void setThisStage(Stage thisStage) {
		this.thisStage = thisStage;
	}
	
	// Windows Titlebar dragable:
	private double dragXOffset;
	private double dragYOffset;
	
	private double dragXMin = 0;
	private double dragYMin = 0;
	
	private double dragXMax = 1920; // TODO dynamically
	private double dragYMax = 1050; // TODO dynamically
	
	/** in pixel */
	private double dragEdgeTolerance = 5;
	
	private EdgeMode edgeMode = EdgeMode.NONE;
	
	public EdgeMode getEdgeMode() {
		return edgeMode;
	}

	public void setEdgeMode(EdgeMode edgeMode) {
		this.edgeMode = edgeMode;
		// TODO change border color
	}

	public void setDragListeners(Stage stage, Node node) {
	
		System.out.println("Drag Events zum Verschieben des Fensters hoeren auf \n"+node);
		node.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if(certainPaneDragableNow && certainPaneDragable) {
					dragXOffset = e.getScreenX() - stage.getX();
					dragYOffset = e.getScreenY() - stage.getY();
				}
			}
		});
		
		node.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				
				if(!certainPaneDragableNow || !certainPaneDragable) return;
				
//				if(((e.getScreenX() - dragXOffset) >= dragXMin) && ((e.getScreenX() - dragXOffset)+stage.getWidth() <= dragXMax)) stage.setX(e.getScreenX() - dragXOffset);
//				if(((e.getScreenY() - dragYOffset) >= dragYMin) && ((e.getScreenY() - dragYOffset)+stage.getHeight() <= dragYMax)) stage.setY(e.getScreenY() - dragYOffset);

				double newX;
				if(((e.getScreenX() - dragXOffset) > dragXMin)) {
					if((e.getScreenX() - dragXOffset)+stage.getWidth() < dragXMax) {
						newX = e.getScreenX() - dragXOffset; 
					}
					else {
						newX = dragXMax-stage.getWidth();
					}
				}
				else {
					newX = dragXMin;
				}
				stage.setX(newX);
				double newY;
				if((e.getScreenY() - dragYOffset) > dragYMin) {
					if(((e.getScreenY() - dragYOffset)+stage.getHeight() < dragYMax)) {

						newY = e.getScreenY() - dragYOffset;
					}
					else {
						newY = dragYMax-stage.getHeight();
					}
				}
				else {
					newY = dragYMin;
				}
				stage.setY(newY);
				
				//				if((e.getScreenX() - dragXOffset)+stage.getWidth() <= dragXMin) stage.setX(e.getScreenX() - dragXOffset);
//				if((e.getScreenY() - dragYOffset)+stage.getHeight() <= dragYMin) stage.setY(e.getScreenY() - dragYOffset);
				
				// TODO find out the order of these if-statements
				if(stage.getX() < (dragXMin+dragEdgeTolerance)) {
					edgeMode = EdgeMode.LEFT;
				}
				else if(stage.getY() < (dragYMin+dragEdgeTolerance)) {
					edgeMode = EdgeMode.TOP;
				}
				else if(stage.getX()+stage.getWidth() > (dragXMax-dragEdgeTolerance)) {
					edgeMode = EdgeMode.RIGHT;
				}
				else if(stage.getY()+stage.getHeight() > (dragYMax-dragEdgeTolerance)) {
					edgeMode = EdgeMode.BOTTOM;
				}
				else {
					edgeMode = EdgeMode.NONE;
				}
				
				System.out.println("Current pos: "+stage.getX()+"x"+stage.getY()+"\tEdgeMode: "+edgeMode);
				afterOnMouseDragged();
			}
		});
		
		node.setOnMouseReleased(e -> {
			onMouseReleased(e);
		});
		
		node.setOnMouseEntered(e -> {
			onMouseEntered(e);
		});
		
		node.setOnMouseExited(e -> {
			onMouseExited(e);
		});
	}
	
	/**
	 * Herausfinden, an welcher Kante/Ecke des angegebenen Panes
	 * der Mauszeiger gerade ist, um den Mauszeiger zu ändern und
	 * das Skalieren in eine bestimmte Richtung bzw. das Verschieben zu ermöglichen
	 * @param p Pane
	 * @param e MouseEvent
	 * @return DragStatus
	 */
	public static DragStatus determinDragStatus(Pane p, MouseEvent e) {

		double xBorderRight = p.getWidth() * (1-resizePercent);
		double xBorderLeft = p.getWidth() * resizePercent;
		double yBorderTop = p.getHeight() * resizePercent;
		double yBorderBottom = p.getHeight() * (1-resizePercent);

		if((e.getX() < xBorderLeft) && (e.getY() < yBorderTop)) return DragStatus.XY_RESIZE_TOPLEFT;
		if((e.getX() > xBorderRight) && (e.getY() < yBorderTop)) return DragStatus.XY_RESIZE_TOPRIGHT;
		if((e.getX() > xBorderRight) && (e.getY() > yBorderBottom)) return DragStatus.XY_RESIZE_BOTTOMRIGHT;
		if((e.getX() < xBorderLeft) && (e.getY() > yBorderBottom)) return DragStatus.XY_RESIZE_BOTTOMLEFT;

		if(e.getX() > xBorderRight) return DragStatus.X_RESIZE_RIGHT;
		if(e.getX() < xBorderLeft) return DragStatus.X_RESIZE_LEFT;
		if(e.getY() < yBorderTop) return DragStatus.Y_RESIZE_TOP;
		if(e.getY() > yBorderBottom) return DragStatus.Y_RESIZE_BOTTOM;
		return DragStatus.NONE;
	}
	
	public void makePaneDragable(AnchorPane pane) {
		makePaneDragable(pane, null);
	}
	
	public void makePaneDragable(AnchorPane pane, Stage stage) {
		
		startRect = new Rectangle(pane.getLayoutX(), pane.getLayoutY(), pane.getWidth(), pane.getHeight());
		// Keine Max-Height/Width setzen, damit das Pane noch resizeable ist
		/*pane.setMinWidth(standardMinWidth);
		pane.setPrefWidth(width);
		pane.setWidth(width);
		pane.setMinHeight(standardMinHeight);
		pane.setPrefHeight(height);
		pane.setHeight(height);
		pane.setLayoutX(x);
		pane.setLayoutY(y);*/

		// TODO setBGColor

		hoverStatus = DragStatus.NONE;
		dragStatus = DragStatus.NONE;

		pane.setOnDragDetected((EventHandler<? super MouseEvent>) e -> {
			
			// Koordinaten des Panes sind relativ zur Pane, deswegen Umwandlung in Koordinatensystem der Scene
			Bounds sceneBounds = pane.localToScene(pane.getLayoutBounds());
			if(debug) {
				System.out.println("Start X-Ursprung des Panes relativ zur Scene: "+sceneBounds.getMinX());
				System.out.println("Start Y-Ursprung des Panes relativ zur Scene: "+sceneBounds.getMinY());
			}
			// Abst�nde vom Mausklick-Punkt zu den Kanten des DragablePanes:
			xDragStart = e.getSceneX() - sceneBounds.getMinX();
			yDragStart = e.getSceneY() - sceneBounds.getMinY();

			if(hoverStatus == DragStatus.NONE) dragStatus = DragStatus.DRAGGING;
			else dragStatus = hoverStatus;
			freigabe = true;
			//pane.onDragDetected(e);
		});

		//setOnDragDetected(value);
		//setOnDragDone(value);
		//setOnDragDropped(value);
		//setOnDragEntered(value);
		//setOnDragExited(value);
		//setOnDragOver(value);

		//setOnMouseDragEntered(value);
		//setOnMouseDragExited(value);
		//setOnMouseDragged(value);
		//setOnMouseDragOver(value);
		//setOnMouseDragReleased(value);
		//setOnMouseEntered(value);
		//setOnMouseExited(value);
		//setOnMouseMoved(value);
		//setOnMousePressed(value);
		//setOnMouseReleased(value);

		pane.setOnMouseReleased((EventHandler<? super MouseEvent>) e -> {
			if(debug) System.out.println("Drag Dropped");
			dragStatus = DragStatus.NONE;
			//pane.onMouseReleased(e);
		});
		pane.setOnMouseMoved((EventHandler<? super MouseEvent>) e -> {
			if(debug) System.out.println("MouseMoved " + e.getX()+"x"+e.getY());
			javafx.scene.input.MouseEvent ev = e;
			hoverStatus = determinDragStatus(pane, ev);
			if(hoverStatus == DragStatus.NONE) certainPaneDragableNow = true;
			else certainPaneDragableNow = false;
			if(debug) System.out.println("Hover status: " + hoverStatus);
			pane.setCursor(hoverStatus.getCursor());
			//pane.onMouseMoved(e);
		});

		pane.setOnMouseDragged((EventHandler<? super MouseEvent>) e -> {

			Rectangle beginRect = new Rectangle(pane.getLayoutX(), pane.getLayoutY(), pane.getWidth(), pane.getHeight());
			boolean resized = false;
			boolean dragged = false;
			if(debug) System.out.println("Dragged -"+dragStatus);
			double d, difference;
			switch(dragStatus) {
			case X_RESIZE_RIGHT: 

				if(!resizeable) break;
				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d <= pane.getMinWidth() || d >= pane.getMaxWidth()) break;
				pane.setPrefWidth(d);
				//pane.setWidth(d);
				//pane.setMaxWidth(d);
				if(stage != null) stage.setWidth(d);
				if(debug) System.out.println("New width:" + d);
				resized = true;
				break;
			case X_RESIZE_LEFT: 

				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d <= pane.getMinWidth() || d >= pane.getMaxWidth()) break;
				difference = d-pane.getWidth();
				pane.setLayoutX(pane.getLayoutX()-difference);
				pane.setPrefWidth(d);
				//pane.setWidth(d);
				//pane.setMaxWidth(d);
				if(stage != null) {
					stage.setX(stage.getX()-difference);
					stage.setWidth(d);
				}
				if(debug) System.out.println("New width: " + d);
				resized = true;
				break;

			case Y_RESIZE_BOTTOM:

				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d <= pane.getMinHeight() || d >= pane.getMaxHeight()) break;
				//pane.setHeight(d);
				//pane.setMaxHeight(d);
				pane.setPrefHeight(d);
				if(stage != null) stage.setHeight(d);
				if(debug) System.out.println("New Height: " + d);
				resized = true;
				break;

			case Y_RESIZE_TOP:

				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();

				if(stage != null) {
					difference = d-stage.getHeight();
				}
				else {
					difference = d-pane.getHeight();
				}
				if(d <= pane.getMinHeight() || d >= pane.getMaxHeight()) break;

				//pane.setHeight(d);
				pane.setPrefHeight(d);
				//pane.setMaxHeight(d);
				if(stage != null) {
					stage.setY(stage.getY()-difference);
					stage.setHeight(d);
				}
				else {
					pane.setLayoutY(pane.getLayoutY()-difference);
				}
				if(debug) System.out.println("New height: " + d);
				resized = true;
				break;
			case XY_RESIZE_TOPLEFT: 
				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();
				if(d > pane.getMinHeight() && d < pane.getMaxHeight()) {
					difference = d-pane.getHeight();
					pane.setLayoutY(pane.getLayoutY()-difference);
					//pane.setHeight(d);
					pane.setPrefHeight(d);
					//pane.setMaxHeight(d);
					if(stage != null) {
						stage.setY(stage.getY()-difference);
						stage.setHeight(d);
					}
					if(debug) System.out.println("New height: " + d);
					resized = true;
				}

				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d > pane.getMinWidth() && d < pane.getMaxWidth()) {
					difference = d-pane.getWidth();
					pane.setLayoutX(pane.getLayoutX()-difference);
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					//pane.setMaxWidth(d);
					if(stage != null) {
						stage.setX(stage.getX()-difference);
						stage.setWidth(d);
					}
					if(debug) System.out.println("New width: " + d);
					resized = true;
				}
				break;
			case XY_RESIZE_TOPRIGHT: 
				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();
				if(d > pane.getMinHeight() && d < pane.getMaxHeight()) {
					difference = d-pane.getHeight();
					pane.setLayoutY(pane.getLayoutY()-difference);
					//pane.setHeight(d);
					pane.setPrefHeight(d);
					//pane.setMaxHeight(d);
					if(stage != null) {
						stage.setY(stage.getY()-difference);
						stage.setHeight(d);
					}
					if(debug) System.out.println("New height: " + d);
					resized = true;
				}

				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d > pane.getMinWidth() && d < pane.getMaxHeight()) {
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					//pane.setMaxWidth(d);
					if(stage != null) {
						stage.setWidth(d);
					}
					if(debug) System.out.println("New width:" + d);
					resized = true;
				}
				break;
			case XY_RESIZE_BOTTOMRIGHT:
				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d > pane.getMinHeight() && d < pane.getMaxHeight()) {
					//pane.setHeight(d);
					//pane.setMaxHeight(d);
					pane.setPrefHeight(d);
					if(stage != null) {
						stage.setHeight(d);
					}
					if(debug) System.out.println("New Height: " + d);
					resized = true;
				}

				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d > pane.getMinWidth() && d < pane.getMaxWidth()) {
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					//pane.setMaxWidth(d);
					if(stage != null) {
						stage.setWidth(d);
					}
					if(debug) System.out.println("New width:" + d);
					resized = true;
				}
				break;
			case XY_RESIZE_BOTTOMLEFT: 
				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d > pane.getMinHeight() && d < pane.getMaxHeight()) {
					//pane.setHeight(d);
					//pane.setMaxHeight(d);
					pane.setPrefHeight(d);
					if(stage != null) {
						stage.setHeight(d);
					}
					if(debug) System.out.println("New Height: " + d);
					resized = true;
				}

				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d > pane.getMinWidth() && d < pane.getMaxWidth()) {
					difference = d-pane.getWidth();
					pane.setLayoutX(pane.getLayoutX()-difference);
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					//pane.setMaxWidth(d);
					if(stage != null) {
						stage.setX(stage.getX()-difference);
						stage.setWidth(d);
					}
					if(debug) System.out.println("New width: " + d);
					resized = true;
				}
				break;
				// TODO eckpunkte vl verschoenern (programmiertechnisch)
			case DRAGGING: 

				if(!dragable) break;

				if(debug) System.out.print("X-Vorher: " + pane.getLayoutX());
				Point2D mousePointLocal = pane.getParent().sceneToLocal(e.getSceneX(), e.getSceneY());
				pane.setLayoutX(mousePointLocal.getX()-xDragStart);
				pane.setLayoutY(mousePointLocal.getY()-yDragStart);
				
				if(debug) System.out.println("\tX-Nachher: " + pane.getLayoutX());
				dragged = true;
				break;
			default:
				break;
			}
			// TODO is enum copied or called by refrence?
			// TODO are prev and new values the same?
			onMouseDragged(e, new ResizeEvent(dragStatus,dragged,resized,beginRect,pane.getWidth(),pane.getHeight(),pane.getLayoutX(),pane.getLayoutY()));
		});
	}
	
	public void onMouseReleased(MouseEvent e) {
		
	}
	
	public void onMouseEntered(MouseEvent e) {
		
	}
	
	public void onMouseExited(MouseEvent e) {
		
	}
	
	public void afterOnMouseDragged() {
		
	}
	
	public void onMouseDragged(MouseEvent e) {
		
	}
	
	public void onMouseDragged(MouseEvent e, ResizeEvent re) {
		
	}

	public boolean isDragable() {
		return dragable;
	}

	public void setDragable(boolean dragable) {
		this.dragable = dragable;
	}

	public boolean isResizeable() {
		return resizeable;
	}

	public void setResizeable(boolean resizeable) {
		this.resizeable = resizeable;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	
	
}