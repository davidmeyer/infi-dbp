package control.enums;

public enum EdgeMode {
	NONE,
	TOP,
	RIGHT,
	BOTTOM,
	LEFT
}
