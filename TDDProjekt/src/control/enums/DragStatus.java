package control.enums;

import javafx.scene.Cursor;

/** Alle Statusse die auftreten k�nnen beim Dragen oder Hovern �ber das DragablePane 
 * Annahme: Koordinatensystem ist links oben angesetzt
 */
public enum DragStatus {
	NONE(Cursor.DEFAULT),
	X_RESIZE_RIGHT(Cursor.E_RESIZE),
	X_RESIZE_LEFT(Cursor.W_RESIZE),
	Y_RESIZE_TOP(Cursor.N_RESIZE),
	Y_RESIZE_BOTTOM(Cursor.S_RESIZE),
	XY_RESIZE_TOPLEFT(Cursor.NW_RESIZE),
	XY_RESIZE_TOPRIGHT(Cursor.NE_RESIZE),
	XY_RESIZE_BOTTOMRIGHT(Cursor.SE_RESIZE),
	XY_RESIZE_BOTTOMLEFT(Cursor.SW_RESIZE),
	DRAGGING(Cursor.DEFAULT);

	private Cursor cursor;

	public Cursor getCursor() { return cursor; }

	DragStatus(Cursor cursor) {
		this.cursor = cursor;
	}
}