package control.enums;

public enum SyncMode {

	SYNCHED,
	SYNCHING,
	SYNCH_FAILED
}