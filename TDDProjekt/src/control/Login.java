package control;

import javafx.stage.Stage;
import model.DBManager;
import model.Person;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.media.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class Login extends SuperController {

	@FXML
	private Pane paneTop;
	
    @FXML
    private Button btnClose;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private PasswordField textFieldPassword;

    @FXML
    private Button btnStart;
    
    @FXML
    private Label labelError;
	
	public Login() {
		super("login");
	}
	
    @FXML
    void btnClose_OA(ActionEvent event) {

    }

    @FXML
    /** Clicked on Start Button
     * Start Button functions as login and register button at the same time
     * If person is not registered yet -> register person account and login
     * If person is already registered -> login person
     * 
     * @param event
     */
    void btnStart_OA(ActionEvent event) {

    	String error = "";
    	String email = textFieldEmail.getText();
    	if(!Person.isEmail(email)) {
    		error += "E-Mail address is not valid";
    	}
    	String password = textFieldPassword.getText();
    	// TODO test for invalid input
    	// TODO test encrypt password
    	
    	if(!error.equals("")) {
    		showErrorMessage(error);
    		return;
    	}
    	
    	Person person = new Person(email, password);
    	try {
    		
			if(DBManager.isPersonRegistered(person.getEmail())) { // Login
				DBManager.loginPerson(person.getEmail(), person.getPassword());
			}
			else { // Register
				Person.setLoggedInPerson(DBManager.createPerson(person));
			}
			getGUIAsStage().close();
			Mainmenue menue = new Mainmenue();
			menue.getGUIAsStage();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	// TODO error handling ?
    	
    	// TODO do stuff
    }
    
    /**
     * Displays a error Message inside a Label on the GUI
     */
    void showErrorMessage(String error) {
    	labelError.setText(error);
    }

	@Override
	public Node getGUIAsNode() {
		Node n = super.getGUIAsNode();
		return n;
	}
	
	@Override
	public Stage getGUIAsStage() {
		super.getGUIAsStage(false, false);
		setDragListeners(getThisStage(), ((Login) sc).paneTop);
		return getThisStage();
	}
	
}