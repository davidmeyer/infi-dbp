package control;

import java.awt.event.ActionEvent;

import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

/** 
 * A smaller version of the control.Note class
 * which is displayed on the note selection screen
 * of the main menu
 * 
 * @see control.Note
 * @see contro.Mainmenue
 */
public class SmallNote extends SuperController {

	@FXML
	TextArea textArea;
	@FXML
	AnchorPane anhchorPane;
	@FXML
	Button btnTrash;
	@FXML
	Button btnEdit;
	@FXML
	Button btnOpen;
	/** Database representation of a note */
	model.Note note;
	
	Mainmenue menu;
	
	public SmallNote() {
		super("smallNote");
	}
	
	public SmallNote(Mainmenue menu, model.Note note) {
		this();
		this.note = note;
		this.menu = menu;
		System.out.println("Small note created for note " + note);
	}
	
	@Override
	public Node getGUIAsNode() {
		Node node = super.getGUIAsNode();
		loadNoteContents();
		((SmallNote) sc).btnTrash.setStyle("-fx-background-image: url('/./view/images/icon_trash.png'); -fx-background-repeat: no-repeat; -fx-background-size: 80% 80%; -fx-background-position: center");
		((SmallNote) sc).btnEdit.setStyle("-fx-background-image: url('/./view/images/icon_edit.png'); -fx-background-repeat: no-repeat; -fx-background-size: 80% 80%; -fx-background-position: center");
		((SmallNote) sc).btnOpen.setStyle("-fx-background-image: url('/./view/images/icon_open.png'); -fx-background-repeat: no-repeat; -fx-background-size: 80% 80%; -fx-background-position: center");
		return node;
	}
	
	/**
	 * Load the contents of a note into this note GUI
	 * (only text for now)
	 */
	public void loadNoteContents() {
		if(note == null) {
			System.out.println("SmallNote content wanted to be displayed, but the note was not set");
			return;
		}
		if(textArea != null) {
			textArea.setText(note.getContentTextPreview());
		}
		else {
			((SmallNote) sc).textArea.setText(note.getContentText());
		}
	}

	/**
	 * @return the database representation of this note
	 */
	public model.Note getNote() {
		return note;
	}

	/** Set the note object to this GUI and load its contents into the GUI
	 * @param note the database representation of this note
	 */
	public void setNote(model.Note note) {
		this.note = note;
		loadNoteContents();
	}
	
	@FXML
	public void deleteNote() {
		
	}
	
	@FXML
	/**
	 * Opens the note in a own note window
	 */
	public void openNote() {
		model.Note n;
		if(note != null) n = note;
		else n = ((SmallNote) sc).note;
		System.out.println("Open note in new note stage with " + n.getContentTextPreview());
		control.Note not = new control.Note(n);
		not.getGUIAsStage();
	}
	
	@FXML
	/**
	 * Opens note inside the main menu for further editing
	 */
	public void editNote() {
		model.Note n = note;
		if(n == null) n = ((SmallNote) sc).getNote();
		if(menu != null) {
			menu.showFullNote(n);
		}
		else {
			((SmallNote) sc).menu.showFullNote(n);
		}
		
	}
	
}