package control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.omg.CORBA.Bounds;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.DBManager;
import model.Notebook;
import model.Person;
import javafx.scene.Cursor;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.fxml.*;

public class Mainmenue extends SuperController {
	@FXML
    private SplitPane splitPane_Main;
    @FXML
    private AnchorPane anchorPane_Main;
    @FXML
    private AnchorPane anchorPane_split_left;
    @FXML
    private GridPane gridPane_note;
    @FXML
    private ScrollPane scrollPane_note;
    @FXML
    private Pane pane_noteHeader;
    @FXML
    private AnchorPane anchorPane_split_right;
    @FXML
    private VBox vbox_notebookList;
    @FXML
    private Label label_notebookTitle;
    @FXML
    private Label label_numberOfNotes;
    @FXML
    private Pane paneAdditionalMenu;
    @FXML
    private Label labelNotes;
    
    private ArrayList<NotebookPane> notebookPanes;
    
    private NotebookPane selectedNotebookPane;
    
    MenuPane createNotebook;
    
	public Mainmenue() {
		super("mainmenue");
		notebookPanes = new ArrayList<NotebookPane>();
		notes = new ArrayList<model.Note>();
		//notes.add(new model.Note(new model.Notebook(), "hallo1", "hallo1"));
		//notes.add(new model.Note(new model.Notebook(), "hallo2", "hallo2"));
	}

	/** Notes which should currently be displayed in the right side of the main menue 
	 * (eg all notes of a certain notebook) */
	private ArrayList<model.Note> notes;
	
	public void addNote(model.Note note) {
		notes.add(note);
		showNotes();
		showNotebookList();
	}
	
	public void showNotebookList() {
		try {
			ArrayList<Notebook> notebooks = DBManager.loadNotebooksWithNotes(Person.getLoggedInPerson());
			System.out.println(notebooks.size() + " notebooks found for the logged in person");
			notebookPanes.clear();
			currentNotebook = null;
			((Mainmenue) sc).vbox_notebookList.getChildren().clear();
			for(int i = 0; i < notebooks.size(); i++) {
				NotebookPane pane = new NotebookPane(this, notebooks.get(i)); 
				notebookPanes.add(pane);
				System.out.println("try to add notebookPane " + pane.getGUIAsNode() + " to notebook list");
				((Mainmenue) sc).vbox_notebookList.getChildren().add(pane.getGUIAsNode());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public NotebookPane getSelectedNotebookPane() {
		return selectedNotebookPane;
	}

	public void setSelectedNotebookPane(NotebookPane selectedNotebookPane) {
		this.selectedNotebookPane = selectedNotebookPane;
	}

	/**
	 * Also shows notes
	 */
	public void showNotebook(model.Notebook notebook) {
		notes = notebook.getNotes();
		currentNotebook = notebook;
		showNotes();
	}
	
	model.Notebook currentNotebook;
	
	/**
	 * Displays the currently selected notes as small notes in the main menu
	 */
	public void showNotes() {
		((Mainmenue) sc).gridPane_note.getChildren().clear();
		((Mainmenue) sc).labelNotes.setText("Notes");
		
		System.out.println("showNotes" + pane_noteHeader);
		int numRows;
		if(notes.size() < 1) {
			Label lbl =  new Label("There are currently no notes inside this notebook");
			((Mainmenue) sc).gridPane_note.addRow(0, lbl);
			numRows = 1;
			GridPane.setColumnSpan(lbl, 2);
		}
		else {
			for(int i = 0; i < notes.size(); i++) {
				if((i%2) == 0) ((Mainmenue) sc).gridPane_note.addRow(i/2);
				SmallNote note = new SmallNote(this, notes.get(i));
				AnchorPane pane = note.getGUIAsAnchorPane();
				//note.setWidth(50);
				//note.setHeight(50);
				((Mainmenue) sc).gridPane_note.add(pane, (i%2), (i/2));
				GridPane.setColumnSpan(pane, 1);
				System.out.println("New small Note added to "+(i/2)+" "+(i%2));
			}	
			numRows = (notes.size()/2)+1;
		}
		Button btn = new Button("Create a new note");
		btn.setOnAction(e-> {
			model.Note n = new model.Note(currentNotebook, "", "");
			try {
				DBManager.createNote(n);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			Notebook curNB = currentNotebook;
			showNotebookList();
			showNotebook(curNB);
		});
		((Mainmenue) sc).gridPane_note.addRow(numRows, btn);
		setNumberOfNotes(notes.size());
	}
	
	public void refreshNotes() {
		//  TODO
		System.out.println("Refreshing notes...");
		if(getSelectedNotebookPane() != null) {
			int nbId = getSelectedNotebookPane().getNotebook().getNotebookId();
			showNotebookList();
			for(int i = 0; i < notebookPanes.size(); i++) {
				if(notebookPanes.get(i).getNotebook().getId() == nbId) {
					notebookPanes.get(i).setSelected(true);
					showNotebook(notebookPanes.get(i).getNotebook());
					break;
				}
			}
		}
	}
	
	/**
	 * Displays a note inside the main menu
	 */
	public void showFullNote(model.Note note) {
		((Mainmenue) sc).gridPane_note.getChildren().clear();
		System.out.println("Display note inside main menu " + note);
		FullNote fullNote = new FullNote(this, note);
		Main.setCurrentFullNote(fullNote);
		((Mainmenue) sc).gridPane_note.addRow(0, fullNote.getGUIAsNode());
		// TODO
	}
	
	public ArrayList<model.Note> getNotes() {
		return notes;
	}
	
	public void showNotebookCreationMenu() {
		GridPane gridPane = (gridPane_note!=null)?gridPane_note:((Mainmenue) sc).gridPane_note;
		gridPane.getChildren().clear();
		CreateNotebook cN = new CreateNotebook(this);
		gridPane.addRow(0, cN.getGUIAsNode());
		((labelNotes!=null)?labelNotes:((Mainmenue) sc).labelNotes).setText("");
		((label_numberOfNotes!=null)?label_numberOfNotes:((Mainmenue) sc).label_numberOfNotes).setText("");
		((label_notebookTitle!=null)?label_notebookTitle:((Mainmenue) sc).label_notebookTitle).setText("Create a new notebook");
	}
	
	@Override
	public Stage getGUIAsStage() {
		Stage stage = super.getGUIAsStage();
		showNotes();
		showNotebookList();
		createNotebook = new MenuPane(this, "Create new Notebook");
		Node createNotebookNode = createNotebook.getGUIAsNode();
		((MenuPane) createNotebook.sc).anchorPane.setOnMouseClicked(e -> {
			resetSelectedNotebooks();
			createNotebook.setSelected(true);
			showNotebookCreationMenu();
		});
		Pane paneAddMenu = (paneAdditionalMenu!=null)?paneAdditionalMenu:((Mainmenue) sc).paneAdditionalMenu;
		paneAddMenu.getChildren().add(createNotebookNode);
		stage.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean notShown, Boolean arg2) {
				if(!notShown) {
					refreshNotes();
				}
			}
		});
		return stage;
	}

	public void resetSelectedNotebooks() {
		for(int i = 0; i < notebookPanes.size(); i++) {
			notebookPanes.get(i).setSelected(false);
		}
		if(createNotebook != null) {
			createNotebook.setSelected(false);
		}
	}
	
	public void setNotebookTitle(String title) {
		if(label_notebookTitle != null) {
			label_notebookTitle.setText(title);
		}
		else {
			((Mainmenue) sc).label_notebookTitle.setText(title);
		}
	}
	
	public void setNumberOfNotes(int amount) {
		String title = "";
		if(amount < 100) title += " ";
		if(amount < 10) title += " ";
		title += amount;
		if(label_numberOfNotes != null) {
			label_numberOfNotes.setText(title+"");
		}
		else {
			((Mainmenue) sc).label_numberOfNotes.setText(title+"");
		}
	}
	
}
