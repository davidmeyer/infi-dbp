package control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * singleton
 *
 */
public class Settings implements Serializable {

	public static final String SETTINGS_FILE_PATH = "C:\\temp\\notz_settings.conf";
	
	private int personId = -1;
	
	private static Settings instance;
	
	private Settings() {
		
	}
	
	public static Settings getInstance() {
		if(instance == null) {
			instance = new Settings();
			loadSettings();
		}
		return instance;
	}
	
	public static void saveSettings() {
		
		FileOutputStream fileStream = null;
		ObjectOutputStream objectStream = null;
		try {
			fileStream = new FileOutputStream(new File(SETTINGS_FILE_PATH));
			objectStream = new ObjectOutputStream(fileStream);
			objectStream.writeObject(instance);
			objectStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(fileStream != null) {
				try {
					fileStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					fileStream = null;
				}
			}
			if(objectStream != null) {
				try {
					objectStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					objectStream = null;
				}
			}
		}
	}
	
	public static void loadSettings() {
		
		FileInputStream inputStream = null;
		ObjectInputStream objectStream = null;
		try {
			inputStream = new FileInputStream(new File(SETTINGS_FILE_PATH));
			objectStream = new ObjectInputStream(inputStream);
			instance = (Settings) objectStream.readObject();
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
			System.out.println("Settings file ("+SETTINGS_FILE_PATH+") was not found");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					inputStream = null;
				}
			}
			if(objectStream != null) {
				try {
					objectStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					objectStream = null;
				}
			}
		}
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}
}