package control;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.fxml.*;
import javafx.scene.control.*;

/**
 * A pane which is displayed in the left notebooks list
 * inside the main menu
 */
public class NotebookPane extends SuperController {

	model.Notebook notebook;
	private boolean selected;
	
	private Mainmenue menu;
	
	@FXML
	Label label;
	@FXML
	AnchorPane anchorPane;
	
	public NotebookPane(Mainmenue menu, model.Notebook notebook) {
		this();
		this.menu = menu;
		this.notebook = notebook;
		selected = false;
	}
	
	public NotebookPane() {
		super("NotebookPane");
	}

	@Override
	public Node getGUIAsNode() {
		Node node = super.getGUIAsNode();
		((NotebookPane) sc).anchorPane.setOnMouseClicked(e -> {
			menu.resetSelectedNotebooks();
			this.setSelected(!this.isSelected());
			if(this.isSelected()) {
				menu.showNotebook(notebook);
			}
		});
		setTitle(notebook.getTitle());
		return node;
	}
	
	public model.Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(model.Notebook notebook) {
		this.notebook = notebook;
		setTitle(notebook.getTitle());
	}

	public void setTitle(String title) {
		if(label != null) {
			label.setText(title);
		}
		else {
			((NotebookPane) sc).label.setText(title);
		}
	}
	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		if(this.selected) {
			menu.setNotebookTitle(this.notebook.getTitle());
			menu.setSelectedNotebookPane(this);
		}
		if(this.anchorPane != null) {
			if(this.selected) {
				anchorPane.getStyleClass().add("notebookPaneSelected");
			}
			else {
				while(true) { // Remove this class as often as neccessary (theoretical this class should only be specified once, but who knows..)
					if(!anchorPane.getStyleClass().remove("notebookPaneSelected")) {
						break;
					}
				}
			}
		}
		else {
			if(this.selected) {
				((NotebookPane) sc).anchorPane.getStyleClass().add("notebookPaneSelected");
			}
			else {
				while(true) { // Remove this class as often as neccessary (theoretical this class should only be specified once, but who knows..)
					if(!((NotebookPane) sc).anchorPane.getStyleClass().remove("notebookPaneSelected")) {
						break;
					}
				}
			}
		}
	}
}