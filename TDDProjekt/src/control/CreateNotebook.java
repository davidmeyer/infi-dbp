package control;

import java.sql.SQLException;

import javafx.fxml.*;
import javafx.scene.control.*;
import model.DBManager;
import model.Notebook;
import model.Person;

public class CreateNotebook extends SuperController {

	@FXML
	private TextField textFieldNotebookName;
	@FXML
	private Button btnCreate;
	
	private Mainmenue menu;
	
	public CreateNotebook() {
		super("CreateNotebook");
	}
	
	public CreateNotebook(Mainmenue menu) {
		this();
		this.menu = menu;
	}

	@FXML
	public void createNotebook() {
		TextField tf = textFieldNotebookName!=null?textFieldNotebookName:((CreateNotebook) sc).textFieldNotebookName;
		System.out.println("logged in " + Person.getLoggedInPerson());
		Notebook nb = new Notebook(Person.getLoggedInPerson(), tf.getText());
		try {
			DBManager.createNotebook(nb);
			(menu!=null?menu:((CreateNotebook) sc).menu).refreshNotes();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	
}