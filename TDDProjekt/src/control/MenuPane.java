package control;

import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.Node;
import javafx.scene.canvas.*;
import javafx.application.*;
import javafx.scene.layout.*;

public class MenuPane extends SuperController {

	@FXML
	Label label;
	@FXML
	AnchorPane anchorPane;
	
	private boolean selected;
	
	Mainmenue menu;
	
	String title = "";
	
	public MenuPane() {
		super("MenuPane");
	}
	
	public MenuPane(Mainmenue menu, String title) {
		this();
		this.title = title;
		this.menu = menu;
	}
	
	@Override
	public Node getGUIAsNode() {
		Node n = super.getGUIAsNode();
		((MenuPane) sc).label.setText(title);
		return n;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
		if(this.selected) {
			menu.setSelectedNotebookPane(null);
		}
		if(this.anchorPane != null) {
			if(this.selected) {
				anchorPane.getStyleClass().add("notebookPaneSelected");
			}
			else {
				while(true) { // Remove this class as often as neccessary (theoretical this class should only be specified once, but who knows..)
					if(!anchorPane.getStyleClass().remove("notebookPaneSelected")) {
						break;
					}
				}
			}
		}
		else {
			if(this.selected) {
				((MenuPane) sc).anchorPane.getStyleClass().add("notebookPaneSelected");
			}
			else {
				while(true) { // Remove this class as often as neccessary (theoretical this class should only be specified once, but who knows..)
					if(!((MenuPane) sc).anchorPane.getStyleClass().remove("notebookPaneSelected")) {
						break;
					}
				}
			}
		}
	}
	
}