package control;

import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import model.DBManager;
import model.Person;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.awt.Color;
import java.sql.SQLException;

import control.enums.EdgeMode;
import control.enums.SyncMode;
import control.listener.ResizeEvent;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.geometry.*;
import javafx.scene.canvas.*;
import javafx.scene.layout.*;
import javafx.animation.Animation;
import javafx.animation.Transition;

public class Note extends SuperController {

	@FXML
	AnchorPane anchorPane;
	@FXML
	Pane paneTitle;
	@FXML
	Button btnPlus;
	@FXML
	Button btnMore;
	@FXML
	Button btnClose;
	@FXML
	Button btnSync;
	@FXML
	TextArea textAreaContent;

	private model.Note note;

	private EdgeMode hidden = EdgeMode.NONE;

	private boolean hoverBlock = false;

	private long timeLastType = 0;
	private boolean savedChanges = true;
	
	private double tempWidth;
	private double tempHeight;

	SyncMode syncMode = SyncMode.SYNCHED;

	public Note() {
		super("note");
		setDebug(true);
		setDragable(false);
		setResizeable(true);
	}

	/** Show the note inside a note GUI */
	public Note(model.Note note) {
		this();
		this.note = note;

		//textAreaContent.setText(note.getContentText());
	}

	/**
	 * TODO WIP 
	 */
	public void minimize() {
		getThisStage().hide(); // doesn't work
	}

	public void setSyncMode(SyncMode syncMode) {
		this.syncMode = syncMode;
		Button btn = btnSync!=null?btnSync:((Note) sc).btnSync;
		switch(syncMode) {
		case SYNCHED:
			btn.setText("*");
			break;
		case SYNCHING: btn.setText("#");
		try {
			if(this.note == null) { // if it is a new note -> insert instead of update
//				Notebook notebook = Person.getLoggedInPerson().getNotebooks().get(0).getId();
				this.note = DBManager.createNote(new model.Note(Person.getLoggedInPerson().getNotebooks().get(0), getContentText(), ""));
			}
			else {
				DBManager.updateNote(this.note);
			}
			setSyncMode(SyncMode.SYNCHED);
			setSavedChanges(true);
			((Note) sc).setSavedChanges(true);
		} catch (ClassNotFoundException e) {
			setSyncMode(SyncMode.SYNCH_FAILED);
			e.printStackTrace();
		} catch (SQLException e) {
			setSyncMode(SyncMode.SYNCH_FAILED);
			e.printStackTrace();
		}
		break;
		case SYNCH_FAILED: btn.setText("F"); break;
		}
	}

	public SyncMode getSyncMode() {
		return syncMode;
	}
	
	/** Get the text which is inside the textArea */
	public String getContentText() {
		if(textAreaContent != null) {
			return textAreaContent.getText();
		}
		else {
			return ((Note) sc).textAreaContent.getText();
		}
	}

	@Override
	public Stage getGUIAsStage() {

		Stage stage = super.getGUIAsStage(true, false);
		setDragListeners(stage, ((Note) sc).paneTitle);
		makePaneDragable(((Note) sc).anchorPane, stage);
		Main.addNoteGUI(this);

		((Note) sc).textAreaContent.setStyle("text-area-background: #FF0000");
		if(note != null) ((Note) sc).textAreaContent.setText(note.getContentText());
		((Note) sc).paneTitle.setStyle("-fx-background-color: #DD0000");

		return stage;
	}

	@Override
	public Node getGUIAsNode() {
		Node node = super.getGUIAsNode();

		return node;
	}

	/*public void setBackgroundColor(Color color) {
		// TODO implement this 
	}*/

	@FXML
	public void btnPlus_OA(ActionEvent e) {

		Note note = new Note();
		Stage noteStage = note.getGUIAsStage();
		noteStage.setWidth(((Note) sc).getThisStage().getWidth());
		noteStage.setHeight(((Note) sc).getThisStage().getHeight());
		noteStage.setX(((Note) sc).getThisStage().getX()+((Note) sc).getThisStage().getWidth()+10);
		//		noteStage.setX(getThisStage().getX());
		noteStage.setY(((Note) sc).getThisStage().getY());
	}

	@FXML
	public void btnMore_OA(ActionEvent e) {

	}

	@FXML
	public void btnSync_OA(ActionEvent e) {
		setSyncMode(SyncMode.SYNCHING);
	}

	// --- HIDING --- 
	@FXML
	/**
	 * When the close button [X] in the upper right corner is pressed
	 * @param e ActionEvent
	 */
	public void btnClose_OA(ActionEvent e) {
		closeNote();
	}

	/** @return Is this note GUI currently hidden?
	 * (on the sides of the screen) */
	public EdgeMode getHidden() {
		return hidden;
	}

	/**
	 * 
	 * @param mode On which side is the note hidden (or not hidden)?
	 * @param animation When hidden mode has changed, should the change be animated (TODO WIP)?
	 */
	public void setHidden(EdgeMode mode, boolean animation) {
		hidden = mode;
		if((hidden == null) || (hidden == EdgeMode.NONE)) return;
		hideAnimation(hidden);
	}

	public void hideAnimation(EdgeMode mode) {

		if((mode == null) || (mode == EdgeMode.NONE)) return;

		final Animation animation = new Transition() {
			double beginValue = 0;
			double residueValue = 10;
			double beginPosValue = 0;
			{
				setCycleDuration(Duration.millis(200));

				if((mode == EdgeMode.LEFT) || (mode == EdgeMode.RIGHT)) {
					beginValue = getThisStage().getWidth();
					tempWidth = beginValue;
					beginPosValue = getThisStage().getX();
				}
				else if((mode == EdgeMode.BOTTOM) || (mode == EdgeMode.TOP)) {
					beginValue = getThisStage().getHeight();
					tempHeight = beginValue;
					beginPosValue = getThisStage().getY();
				}
			}

			@Override
			protected void interpolate(double frac) {
				if((mode == EdgeMode.LEFT) || (mode == EdgeMode.RIGHT)) {
					getThisStage().setWidth(beginValue + residueValue*frac - beginValue*frac);
					if(mode == EdgeMode.RIGHT) getThisStage().setX(beginPosValue - residueValue*frac + beginValue*frac);
				}
				else if((mode == EdgeMode.BOTTOM) || (mode == EdgeMode.TOP)) {
					getThisStage().setHeight(residueValue*frac + beginValue - beginValue*frac);
					if(mode == EdgeMode.BOTTOM) getThisStage().setY(beginPosValue - (residueValue*2*frac) + beginValue*frac);
				}
				if(frac == 1.0) hoverBlock = false; // TODO
			}
		};
		animation.play();
	}

	public void showAnimation(EdgeMode mode) {

		final Animation animation = new Transition() {
			double endValue = 500; // Without Residue
			double residueValue = 10;
			double beginPosValue = 0;
			{
				setCycleDuration(Duration.millis(200));

				if((mode == EdgeMode.LEFT) || (mode == EdgeMode.RIGHT)) {
					endValue = tempWidth;
					beginPosValue = getThisStage().getX();
				}
				else if((mode == EdgeMode.BOTTOM) || (mode == EdgeMode.TOP)) {
					endValue = tempHeight;
					beginPosValue = getThisStage().getY();
				}
			}

			@Override
			protected void interpolate(double frac) {
				if((mode == EdgeMode.LEFT) || (mode == EdgeMode.RIGHT)) {
					getThisStage().setWidth(residueValue*frac + endValue*frac);
					if(mode == EdgeMode.RIGHT) getThisStage().setX(beginPosValue - (residueValue*frac + endValue*frac));
				}
				else if((mode == EdgeMode.BOTTOM) || (mode == EdgeMode.TOP)) {
					getThisStage().setHeight(residueValue + endValue*frac);
					if(mode == EdgeMode.BOTTOM) getThisStage().setY(beginPosValue + residueValue*frac - endValue*frac);
				}
			}
		};
		animation.play();
	}

	@Override
	public void onMouseReleased(MouseEvent e) {

		hoverBlock = true;
		setHidden(getEdgeMode(), true);
		super.onMouseReleased(e);
	}

	@Override
	public void onMouseEntered(MouseEvent e) {

		if(!hoverBlock && (getHidden() != EdgeMode.NONE)) showAnimation(getHidden());
		super.onMouseEntered(e);
	}

	@Override
	public void onMouseExited(MouseEvent e) {

		if(!hoverBlock && (getHidden() != EdgeMode.NONE)) hideAnimation(getHidden());
		super.onMouseExited(e);
	}

	@Override
	public void afterOnMouseDragged() {
		if(getEdgeMode() == EdgeMode.NONE) setHidden(EdgeMode.NONE, false);
		super.afterOnMouseDragged();
	}

	// --- HIDING END ---

	public void setWidth(double width) {
		//double prevWidth = anchorPane.getWidth();
		((Note) sc).anchorPane.setPrefWidth(width);
		((Note) sc).anchorPane.setMinWidth(width);
		((Note) sc).anchorPane.setMaxWidth(width);

		updateTitlePaneWidth(width);
	}

	public void setHeight(double height) {
		//double prevWidth = anchorPane.getWidth();
		((Note) sc).anchorPane.setPrefHeight(height);
		((Note) sc).anchorPane.setMinHeight(height);
		((Note) sc).anchorPane.setMaxHeight(height);

		((Note) sc).textAreaContent.setMaxHeight(height-((Note) sc).paneTitle.getHeight());
		((Note) sc).textAreaContent.setMinHeight(height-((Note) sc).paneTitle.getHeight());
		((Note) sc).textAreaContent.setPrefHeight(height-((Note) sc).paneTitle.getHeight());
	}
	
	public void updateTitlePaneWidth(double width) {
		System.out.println("Updating Title Pane Width");
		((Note) sc).paneTitle.setPrefWidth(width);
		((Note) sc).paneTitle.setMinWidth(width);
		((Note) sc).paneTitle.setMaxWidth(width);
	}

	@Override
	public void onMouseDragged(MouseEvent e, ResizeEvent re) {
		updateTitlePaneWidth(re.getNewWidth());
	}

	public long getTimeLastType() {
		return ((Note) sc).timeLastType;
	}

	protected void setTimeLastType(long timeLastType) {
		this.timeLastType = timeLastType;
	}

	public boolean isSavedChanges() {
		return ((Note) sc).savedChanges;
	}

	public void setSavedChanges(boolean savedChanges) {
		this.savedChanges = savedChanges;
	}

	public model.Note getNote() {
		return note;
	}

	public void setNote(model.Note note) {
		this.note = note;
	}

	/**
	 * Close note GUI
	 * TODO force save note
	 */
	public void closeNote() {
		((Note) sc).getThisStage().close();
		Main.removeNoteGUI(this);
		Main.removeNoteGUI(((Note) sc));
	}


	@FXML
	/** When something was typed in the textArea,
	 * register that changes were made so that they can be
	 * saved in the database when the next note-change-poll happens
	 * (from Timer in Main)
	 */
	public void keyTyped() {
		setSavedChanges(false);
		setTimeLastType(System.currentTimeMillis());
	}

	@Override
	public String toString() {
		if(note != null) {
			return "Note GUI for " + note.getId();
		}
		return "Note GUI for undefined note";
	}
}