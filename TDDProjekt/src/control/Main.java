package control;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import control.enums.SyncMode;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import model.DBManager;
import model.Person;

public class Main extends Application {

	private static ArrayList<Note> noteGUIs;
	private static Timer timer;
	private static FullNote currentFullNote;
	
	private static final long noteChangesPollingTime = 3000;
	
	public static void main(String[] args) {
		
		System.out.println("Starting Notz Client...");
		noteGUIs = new ArrayList<Note>();
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				System.out.println("Polling note changes for " + noteGUIs.size() + " notes...");
				// Notes that are opened in a separate window
				for(int i = 0; i < noteGUIs.size(); i++) {
					if(noteGUIs.get(i) == null) break; // TODO remove from list?
					if(!noteGUIs.get(i).isSavedChanges() && ((noteGUIs.get(i).getTimeLastType()+noteChangesPollingTime) <= System.currentTimeMillis())) {
						System.out.println("Attempting to save changes of " + noteGUIs.get(i));
						Note notegui = noteGUIs.get(i);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								notegui.setSyncMode(SyncMode.SYNCHING);
							}
						});
//						noteGUIs.get(i).getNote() TODO update note in DB
					}
				}
				// Note that is currently edited whithin the main menu
				if(currentFullNote != null) {
					if(!currentFullNote.isSavedChanges() && (currentFullNote.getTimeLastType()+noteChangesPollingTime) <= System.currentTimeMillis()) {
						System.out.println("Attempting to save full note " + currentFullNote.getNote());
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								currentFullNote.setSyncMode(SyncMode.SYNCHING);
							}
						});
					}
				}
				else {
					System.out.println("No current Full Note");
				}
				System.out.println("Polling note changes done!");
			}
		}, noteChangesPollingTime, noteChangesPollingTime);
		
		launch(args);
		try {
			DBManager.getConnection();
			// TODO show in GUI error message
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		if(Settings.getInstance().getPersonId() != -1) { // Person is serialized in local file
			Person p = DBManager.loadPerson(Settings.getInstance().getPersonId()); // TODO
			Person.setLoggedInPerson(DBManager.loadPersonWithContent(p));
		}
		
		if(Person.getLoggedInPerson() == null) { // No person is logged in right now -> prompt login window
			Login login = new Login();
			login.getGUIAsStage();
		}
		else { // user is logged in
			// do all of the under
			// TODO dynamically load notes
			
			Mainmenue menue = new Mainmenue();
			menue.getGUIAsStage();
			//menue.showNotes();
		}
	}

	@Override
	/**
	 * TODO is this the right method?
	 */
	public void stop() throws Exception {
		Settings.saveSettings();
		super.stop();
	}
	
	public static void addNoteGUI(Note note) {
		noteGUIs.add(note);
	}
	
	public static void removeNoteGUI(Note note) {
		noteGUIs.remove(note);
	}

	public static FullNote getCurrentFullNote() {
		return currentFullNote;
	}
	public static void setCurrentFullNote(FullNote currentFullNote) {
		Main.currentFullNote = currentFullNote;
	}
}