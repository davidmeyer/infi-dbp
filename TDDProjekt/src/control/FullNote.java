package control;

import java.sql.SQLException;

import control.enums.SyncMode;
import javafx.fxml.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import model.DBManager;
import model.Person;

public class FullNote extends SuperController {

	@FXML
	TextArea textArea;
	@FXML
	Button btnBack;
	@FXML
	Label labelSynch;
	
	private model.Note note;
	
	private SyncMode syncMode;
	
	Mainmenue menu;
	
	private long timeLastType = 0;
	private boolean savedChanges = true;
	
	public FullNote() {
		super("fullNote");
	}
	
	public FullNote(Mainmenue menu, model.Note note) {
		this();
		this.note = note;
		this.menu = menu;
	}
	
	@Override
	public Node getGUIAsNode() {
		Node node = super.getGUIAsNode();
		if(note != null) {
			 setText(note.getContentText());
		}
		((FullNote) sc).btnBack.setStyle("-fx-background-image: url('/./view/images/icon_arrow.png'); -fx-background-repeat: no-repeat; -fx-background-size: 50% 50%; -fx-background-position: center");
		return node;
	}
	
	public void setText(String text) {
		if(textArea != null) {
			textArea.setText(text);
		}
		else {
			((FullNote) sc).textArea.setText(text);
		}
	}
	
	public String getText() {
		if(textArea != null) {
			return textArea.getText();
		}
		else {
			return ((FullNote) sc).textArea.getText();
		}
	}
	
	@FXML
	public void goBack() {
		Main.setCurrentFullNote(null);
		if(menu != null) {
			menu.showNotes();
		}
		else {
			((FullNote) sc).menu.showNotes();
		}
	}
	
	@FXML
	public void keyTyped() {
		setSavedChanges(false);
		setTimeLastType(System.currentTimeMillis());
		((FullNote) sc).setSavedChanges(false);
		((FullNote) sc).setTimeLastType(System.currentTimeMillis());
	}
	
	public void setSyncMode(SyncMode syncMode) {
		this.syncMode = syncMode;
		Label lbl = (labelSynch!=null)?labelSynch:((FullNote) sc).labelSynch;
		switch(syncMode) {
		case SYNCHED:
			lbl.setText("Synched");
			break;
		case SYNCHING: lbl.setText("Synching...");
		try {
			if(this.note == null) { // if it is a new note -> insert instead of update
//				Notebook notebook = Person.getLoggedInPerson().getNotebooks().get(0).getId();
				this.note = DBManager.createNote(new model.Note(Person.getLoggedInPerson().getNotebooks().get(0), getText(), ""));
			}
			else {
				this.note.setContentText(getText());
				System.out.println("Updating note " + this.note);
				DBManager.updateNote(this.note);
			}
			setSyncMode(SyncMode.SYNCHED);
			setSavedChanges(true);
			((FullNote) sc).setSavedChanges(true);
		} catch (ClassNotFoundException e) {
			setSyncMode(SyncMode.SYNCH_FAILED);
			e.printStackTrace();
		} catch (SQLException e) {
			setSyncMode(SyncMode.SYNCH_FAILED);
			e.printStackTrace();
		}
		break;
		case SYNCH_FAILED: lbl.setText("Synching failed"); break;
		}
	}

	public long getTimeLastType() {
		return timeLastType;
	}

	public void setTimeLastType(long timeLastType) {
		this.timeLastType = timeLastType;
	}

	public boolean isSavedChanges() {
		return savedChanges;
	}

	public void setSavedChanges(boolean savedChanges) {
		this.savedChanges = savedChanges;
		if(!this.savedChanges) {
			Label lbl = (labelSynch!=null)?labelSynch:((FullNote )sc).labelSynch;
			lbl.setText("Not synched");
		}
	}

	public model.Note getNote() {
		return note;
	}
	
	
}