package control.listener;

import control.enums.DragStatus;
import javafx.scene.shape.Rectangle;

/**
 * Better name would be "ResizeAndOrDragEvent", but idk/idc
 *
 */
public class ResizeEvent {

	private DragStatus dragStatus;
	private boolean dragged;
	private boolean resized;
	private double previousWidth;
	private double previousHeight;
	private double newWidth;
	private double newHeight;
	private double previousX;
	private double previousY;
	private double newX;
	private double newY;
	// TODO add Time
	
	public ResizeEvent(DragStatus status, boolean dragged, boolean resized, double prevWidth, double prevHeight, double newWidth, double newHeight, double prevX, double prevY, double newX, double newY) {
		dragStatus = status;
		this.dragged = dragged;
		this.resized = resized;
		this.previousWidth = prevWidth;
		this.previousHeight = prevHeight;
		this.newWidth = newWidth;
		this.newHeight = newHeight;
		this.previousX = prevX;
		this.previousY = prevY;
		this.newX = newX;
		this.newY = newY;
	}
	
	public ResizeEvent(DragStatus status, boolean dragged, boolean resized, Rectangle prevRect, Rectangle newRect) {
		this(status, dragged, resized, prevRect, newRect.getWidth(), newRect.getHeight(), newRect.getX(), newRect.getY());
	}
	
	public ResizeEvent(DragStatus status, boolean dragged, boolean resized, Rectangle prevRect, double newWidth, double newHeight, double newX, double newY) {
		this(status, dragged, resized, prevRect.getWidth(), prevRect.getHeight(), newWidth, newHeight, prevRect.getX(), prevRect.getY(), newX, newY);
	}
	// TODO other constructorS?
	
	public DragStatus getDragStatus() {
		return dragStatus;
	}
	public boolean isDragged() {
		return dragged;
	}
	public boolean isResized() {
		return resized;
	}
	public double getPreviousWidth() {
		return previousWidth;
	}
	public double getPreviousHeight() {
		return previousHeight;
	}
	public double getNewWidth() {
		return newWidth;
	}
	public double getNewHeight() {
		return newHeight;
	}
	public double getPreviousX() {
		return previousX;
	}
	public double getPreviousY() {
		return previousY;
	}
	public double getNewX() {
		return newX;
	}
	public double getNewY() {
		return newY;
	}
}