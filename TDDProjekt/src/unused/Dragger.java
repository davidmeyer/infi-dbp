package unused;

import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.Cursor;

/**
 * Objects of this class can be implemented into all 
 * UIs to make them dragable and resizeable.
 *
 */
public class Dragger {

	AnchorPane pane;

	/** Sollen Debug-Nachrichten in die Konsole geschrieben werden? */
	private boolean debug = true;
	
	public static final float borderPercent = 0.06f;

	public static final int standardMinHeight = 10;
	public static final int standardMinWidth = 10;

	// 
	/** Alle Statusse die auftreten k�nnen beim Dragen oder Hovern �ber das DragablePane 
	 * Annahme: Koordinatensystem ist links oben angesetzt
	 */
	public static enum DragStatus {
		NONE(Cursor.DEFAULT),
		X_RESIZE_RIGHT(Cursor.E_RESIZE),
		X_RESIZE_LEFT(Cursor.W_RESIZE),
		Y_RESIZE_TOP(Cursor.N_RESIZE),
		Y_RESIZE_BOTTOM(Cursor.S_RESIZE),
		XY_RESIZE_TOPLEFT(Cursor.NW_RESIZE),
		XY_RESIZE_TOPRIGHT(Cursor.NE_RESIZE),
		XY_RESIZE_BOTTOMRIGHT(Cursor.SE_RESIZE),
		XY_RESIZE_BOTTOMLEFT(Cursor.SW_RESIZE),
		DRAGGING(Cursor.DEFAULT);

		private Cursor cursor;

		public Cursor getCursor() { return cursor; }

		DragStatus(Cursor cursor) {
			this.cursor = cursor;
		}
	}

	/** Status des Drags */
	public DragStatus dragStatus;
	/** Status des Hovers */
	public DragStatus hoverStatus;
	/** X-Abstand zwischen der Position des Mausklicks
	 * und dem (linken) Rand des DragablePanes
	 * (Wert nur sinnvoll w�hrend des Dragens) */
	double xDragStart;
	/** Y-Abstand zwischen der Position des Mausklicks
	 * und dem (oberen) Rand des DragablePanes
	 * (Wert nur sinnvoll w�hrend des Dragens) */
	double yDragStart;

	private static int startWidth = 100;
	private static int startHeight = 100;

	private static Color startColor = new Color(0.5, 0.5, 0.5, 0.4);

	/** Kann die Position durch klicken und ziehen verändert werden?
	 * (nur positionieren, nicht skalieren) */
	private boolean dragable = true;
	private boolean resizeable = true;
	private boolean selectable = true;
	private boolean selected = false;
	
	private boolean freigabe = false;

	/** Startposition und Skalierung */
	protected Rectangle startRect;
	
	public Dragger(AnchorPane pane) {
		 this.pane = pane;
	}
	
	/**
	 * Herausfinden, an welcher Kante/Ecke des angegebenen Panes
	 * der Mauszeiger gerade ist, um den Mauszeiger zu ändern und
	 * das Skalieren in eine bestimmte Richtung bzw. das Verschieben zu ermöglichen
	 * @param p Pane
	 * @param e MouseEvent
	 * @return DragStatus
	 */
	public static DragStatus determinDragStatus(Pane p, MouseEvent e) {

		double xBorderRight = p.getWidth() * (1-borderPercent);
		double xBorderLeft = p.getWidth() * borderPercent;
		double yBorderTop = p.getHeight() * borderPercent;
		double yBorderBottom = p.getHeight() * (1-borderPercent);

		if((e.getX() < xBorderLeft) && (e.getY() < yBorderTop)) return DragStatus.XY_RESIZE_TOPLEFT;
		if((e.getX() > xBorderRight) && (e.getY() < yBorderTop)) return DragStatus.XY_RESIZE_TOPRIGHT;
		if((e.getX() > xBorderRight) && (e.getY() > yBorderBottom)) return DragStatus.XY_RESIZE_BOTTOMRIGHT;
		if((e.getX() < xBorderLeft) && (e.getY() > yBorderBottom)) return DragStatus.XY_RESIZE_BOTTOMLEFT;

		if(e.getX() > xBorderRight) return DragStatus.X_RESIZE_RIGHT;
		if(e.getX() < xBorderLeft) return DragStatus.X_RESIZE_LEFT;
		if(e.getY() < yBorderTop) return DragStatus.Y_RESIZE_TOP;
		if(e.getY() > yBorderBottom) return DragStatus.Y_RESIZE_BOTTOM;
		return DragStatus.NONE;
	}
	
	public void makePaneDragable(AnchorPane pane) {
		
		// TODO etweder BetterPane oder Pane mitgebbar,
		// wenn betterPane -> listener nicht ueberschreiben
		// wenn pane -> listener muessen ueberschrieben werden
		
		startRect = new Rectangle(pane.getLayoutX(), pane.getLayoutY(), pane.getWidth(), pane.getHeight());
		// Keine Max-Height/Width setzen, damit das Pane noch resizeable ist
		/*pane.setMinWidth(standardMinWidth);
		pane.setPrefWidth(width);
		pane.setWidth(width);
		pane.setMinHeight(standardMinHeight);
		pane.setPrefHeight(height);
		pane.setHeight(height);
		pane.setLayoutX(x);
		pane.setLayoutY(y);*/

		// TODO setBGColor

		hoverStatus = DragStatus.NONE;
		dragStatus = DragStatus.NONE;

		pane.setOnDragDetected((EventHandler<? super MouseEvent>) e -> {

			// Koordinaten des Panes sind relativ zur Pane, deswegen Umwandlung in Koordinatensystem der Scene
			Bounds sceneBounds = pane.localToScene(pane.getLayoutBounds());
			if(debug) {
				System.out.println("Start X-Ursprung des Panes relativ zur Scene: "+sceneBounds.getMinX());
				System.out.println("Start Y-Ursprung des Panes relativ zur Scene: "+sceneBounds.getMinY());
			}
			// Abst�nde vom Mausklick-Punkt zu den Kanten des DragablePanes:
			xDragStart = e.getSceneX() - sceneBounds.getMinX();
			yDragStart = e.getSceneY() - sceneBounds.getMinY();

			if(hoverStatus == DragStatus.NONE) dragStatus = DragStatus.DRAGGING;
			else dragStatus = hoverStatus;
			freigabe = true;
			//pane.onDragDetected(e);
		});

		//setOnDragDetected(value);
		//setOnDragDone(value);
		//setOnDragDropped(value);
		//setOnDragEntered(value);
		//setOnDragExited(value);
		//setOnDragOver(value);

		//setOnMouseDragEntered(value);
		//setOnMouseDragExited(value);
		//setOnMouseDragged(value);
		//setOnMouseDragOver(value);
		//setOnMouseDragReleased(value);
		//setOnMouseEntered(value);
		//setOnMouseExited(value);
		//setOnMouseMoved(value);
		//setOnMousePressed(value);
		//setOnMouseReleased(value);

		pane.setOnMouseReleased((EventHandler<? super MouseEvent>) e -> {
			if(debug) System.out.println("Drag Dropped");
			dragStatus = DragStatus.NONE;
			//pane.onMouseReleased(e);
		});
		pane.setOnMouseMoved((EventHandler<? super MouseEvent>) e -> {
			if(debug) System.out.println("MouseMoved " + e.getX()+"x"+e.getY());
			javafx.scene.input.MouseEvent ev = e;
			hoverStatus = determinDragStatus(pane, ev);
			if(debug) System.out.println("Hover status: " + hoverStatus);
			pane.setCursor(hoverStatus.getCursor());
			//pane.onMouseMoved(e);
		});


		pane.setOnMouseDragged((EventHandler<? super MouseEvent>) e -> {

			if(debug) System.out.println("Dragged -"+dragStatus);
			double d;
			double difference;
			switch(dragStatus) {
			case X_RESIZE_RIGHT: 

				if(!resizeable) break;
				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d < pane.getMinWidth()) break;
				pane.setPrefWidth(d);
				//pane.setWidth(d);
				pane.setMaxWidth(d);
				if(debug) System.out.println("New width:" + d);
				break;
			case X_RESIZE_LEFT: 

				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d < pane.getMinWidth()) break;
				difference = d-pane.getWidth();
				pane.setLayoutX(pane.getLayoutX()-difference);
				pane.setPrefWidth(d);
				//pane.setWidth(d);
				pane.setMaxWidth(d);
				if(debug) System.out.println("New width: " + d);
				break;

			case Y_RESIZE_BOTTOM:

				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d < pane.getMinHeight()) break;
				//pane.setHeight(d);
				pane.setMaxHeight(d);
				pane.setPrefHeight(d);
				if(debug) System.out.println("New Height: " + d);
				break;

			case Y_RESIZE_TOP:

				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();

				difference = d-pane.getHeight();
				if(d < pane.getMinHeight()) break;
				pane.setLayoutY(pane.getLayoutY()-difference);
				//pane.setHeight(d);
				pane.setPrefHeight(d);
				pane.setMaxHeight(d);
				if(debug) System.out.println("New height: " + d);
				break;
			case XY_RESIZE_TOPLEFT: 
				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();
				if(d >= pane.getMinHeight()) {
					difference = d-pane.getHeight();
					pane.setLayoutY(pane.getLayoutY()-difference);
					//pane.setHeight(d);
					pane.setPrefHeight(d);
					pane.setMaxHeight(d);
					if(debug) System.out.println("New height: " + d);
				}

				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d >= pane.getMinWidth()) {
					difference = d-pane.getWidth();
					pane.setLayoutX(pane.getLayoutX()-difference);
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					pane.setMaxWidth(d);
					if(debug) System.out.println("New width: " + d);
				}
				break;
			case XY_RESIZE_TOPRIGHT: 
				if(!resizeable) break;
				d = pane.localToScene(pane.getBoundsInLocal()).getMaxY() - e.getSceneY();
				if(d >= pane.getMinHeight()) {
					difference = d-pane.getHeight();
					pane.setLayoutY(pane.getLayoutY()-difference);
					//pane.setHeight(d);
					pane.setPrefHeight(d);
					pane.setMaxHeight(d);
					if(debug) System.out.println("New height: " + d);
				}

				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d >= pane.getMinWidth()) {
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					pane.setMaxWidth(d);
					if(debug) System.out.println("New width:" + d);
				}
				break;
			case XY_RESIZE_BOTTOMRIGHT:
				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d >= pane.getMinHeight()) {
					//pane.setHeight(d);
					pane.setMaxHeight(d);
					pane.setPrefHeight(d);
					if(debug) System.out.println("New Height: " + d);
				}

				d = e.getSceneX() - pane.localToScene(pane.getBoundsInLocal()).getMinX();
				if(d >= pane.getMinWidth()) {
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					pane.setMaxWidth(d);
					if(debug) System.out.println("New width:" + d);
				}
				break;
			case XY_RESIZE_BOTTOMLEFT: 
				if(!resizeable) break;
				d = e.getSceneY() - pane.localToScene(pane.getBoundsInLocal()).getMinY();
				if(d >= pane.getMinHeight()) {
					//pane.setHeight(d);
					pane.setMaxHeight(d);
					pane.setPrefHeight(d);
					if(debug) System.out.println("New Height: " + d);
				}

				d = pane.localToScene(pane.getBoundsInLocal()).getMaxX() - e.getSceneX();
				if(d >= pane.getMinWidth()) {
					difference = d-pane.getWidth();
					pane.setLayoutX(pane.getLayoutX()-difference);
					pane.setPrefWidth(d);
					//pane.setWidth(d);
					pane.setMaxWidth(d);
					if(debug) System.out.println("New width: " + d);
				}
				break;
				// TODO eckpunkte vl verschoenern (programmiertechnisch)
			case DRAGGING: 

				if(!dragable) break;

				if(debug) System.out.print("X-Vorher: " + pane.getLayoutX());
				Point2D mousePointLocal = pane.getParent().sceneToLocal(e.getSceneX(), e.getSceneY());
				pane.setLayoutX(mousePointLocal.getX()-xDragStart);
				pane.setLayoutY(mousePointLocal.getY()-yDragStart);
				
				if(debug) System.out.println("\tX-Nachher: " + pane.getLayoutX());
				break;
			default:
				break;
			}
			//pane.onMouseDragged(e);
		});
	}

	public boolean isDragable() {
		return dragable;
	}

	public void setDragable(boolean dragable) {
		this.dragable = dragable;
	}

	public boolean isResizeable() {
		return resizeable;
	}

	public void setResizeable(boolean resizeable) {
		this.resizeable = resizeable;
	}
	
}