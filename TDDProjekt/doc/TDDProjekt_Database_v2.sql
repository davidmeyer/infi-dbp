--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-21 13:42:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 32775)
-- Name: note; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE note (
    noteid integer NOT NULL,
    notebook integer NOT NULL,
    contenttext text,
    title text
);


ALTER TABLE note OWNER TO admin;

--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE note; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE note IS 'Notiz';


--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN note.contenttext; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON COLUMN note.contenttext IS 'Inhalt';


--
-- TOC entry 185 (class 1259 OID 32773)
-- Name: note_noteid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE note_noteid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE note_noteid_seq OWNER TO admin;

--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 185
-- Name: note_noteid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE note_noteid_seq OWNED BY note.noteid;


--
-- TOC entry 198 (class 1259 OID 32903)
-- Name: notebook; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE notebook (
    notebookid integer NOT NULL,
    author integer NOT NULL,
    title text,
    creationdate timestamp without time zone DEFAULT now()
);


ALTER TABLE notebook OWNER TO admin;

--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE notebook; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE notebook IS 'Notizbuch';


--
-- TOC entry 197 (class 1259 OID 32901)
-- Name: notebook_notebookid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE notebook_notebookid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notebook_notebookid_seq OWNER TO admin;

--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 197
-- Name: notebook_notebookid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE notebook_notebookid_seq OWNED BY notebook.notebookid;


--
-- TOC entry 194 (class 1259 OID 32845)
-- Name: notechange; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE notechange (
    note integer NOT NULL,
    person integer NOT NULL,
    pointintime timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE notechange OWNER TO admin;

--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE notechange; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE notechange IS 'Aenderung einer Notiz. Erste Aenderung entspricht Erstellung der Notiz';


--
-- TOC entry 195 (class 1259 OID 32864)
-- Name: notexpersongroup; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE notexpersongroup (
    persongroup integer NOT NULL,
    contentid integer NOT NULL,
    isnote boolean NOT NULL,
    cansee boolean DEFAULT false NOT NULL,
    canedit boolean NOT NULL
);


ALTER TABLE notexpersongroup OWNER TO admin;

--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 195
-- Name: TABLE notexpersongroup; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE notexpersongroup IS 'Rechtevergabe: Wenn nicht vorhanden -> erben von notebook';


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN notexpersongroup.contentid; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON COLUMN notexpersongroup.contentid IS 'NotizId oder NotebookId';


--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN notexpersongroup.isnote; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON COLUMN notexpersongroup.isnote IS 'true=contentId ist von Notiz. false=contentId ist von Notebook';


--
-- TOC entry 196 (class 1259 OID 32870)
-- Name: notextag; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE notextag (
    tag integer NOT NULL,
    contentid integer NOT NULL,
    isnote boolean NOT NULL
);


ALTER TABLE notextag OWNER TO admin;

--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE notextag; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE notextag IS 'Gruppieren von Notizen/Notizbuechern';


--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN notextag.isnote; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON COLUMN notextag.isnote IS 'true=contentId ist von Notiz. false=contentId ist von Notebook';


--
-- TOC entry 190 (class 1259 OID 32820)
-- Name: person; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE person (
    personid integer NOT NULL,
    email character varying(100) NOT NULL,
    title text,
    firstname text,
    lastname text,
    birthday date,
    creationdate timestamp without time zone
);


ALTER TABLE person OWNER TO admin;

--
-- TOC entry 189 (class 1259 OID 32818)
-- Name: person_personid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE person_personid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE person_personid_seq OWNER TO admin;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 189
-- Name: person_personid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE person_personid_seq OWNED BY person.personid;


--
-- TOC entry 192 (class 1259 OID 32831)
-- Name: persongroup; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE persongroup (
    persongroupid integer NOT NULL,
    title text
);


ALTER TABLE persongroup OWNER TO admin;

--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE persongroup; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE persongroup IS 'Gruppe von Personen';


--
-- TOC entry 191 (class 1259 OID 32829)
-- Name: persongroup_persongroupid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE persongroup_persongroupid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE persongroup_persongroupid_seq OWNER TO admin;

--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 191
-- Name: persongroup_persongroupid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE persongroup_persongroupid_seq OWNED BY persongroup.persongroupid;


--
-- TOC entry 193 (class 1259 OID 32840)
-- Name: personxpersongroup; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE personxpersongroup (
    persongroup integer NOT NULL,
    person integer NOT NULL
);


ALTER TABLE personxpersongroup OWNER TO admin;

--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 193
-- Name: TABLE personxpersongroup; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE personxpersongroup IS 'M-N fuer person und personGroup';


--
-- TOC entry 188 (class 1259 OID 32809)
-- Name: tag; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE tag (
    tagid integer NOT NULL,
    tag text NOT NULL
);


ALTER TABLE tag OWNER TO admin;

--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE tag; Type: COMMENT; Schema: public; Owner: admin
--

COMMENT ON TABLE tag IS 'Tag/Schlagwort zum Gruppieren';


--
-- TOC entry 187 (class 1259 OID 32807)
-- Name: tag_tagid_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE tag_tagid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tag_tagid_seq OWNER TO admin;

--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 187
-- Name: tag_tagid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE tag_tagid_seq OWNED BY tag.tagid;


--
-- TOC entry 2046 (class 2604 OID 32778)
-- Name: note noteid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY note ALTER COLUMN noteid SET DEFAULT nextval('note_noteid_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 32906)
-- Name: notebook notebookid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notebook ALTER COLUMN notebookid SET DEFAULT nextval('notebook_notebookid_seq'::regclass);


--
-- TOC entry 2048 (class 2604 OID 32823)
-- Name: person personid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY person ALTER COLUMN personid SET DEFAULT nextval('person_personid_seq'::regclass);


--
-- TOC entry 2049 (class 2604 OID 32834)
-- Name: persongroup persongroupid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY persongroup ALTER COLUMN persongroupid SET DEFAULT nextval('persongroup_persongroupid_seq'::regclass);


--
-- TOC entry 2047 (class 2604 OID 32812)
-- Name: tag tagid; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY tag ALTER COLUMN tagid SET DEFAULT nextval('tag_tagid_seq'::regclass);


--
-- TOC entry 2203 (class 0 OID 32775)
-- Dependencies: 186
-- Data for Name: note; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY note (noteid, notebook, contenttext, title) FROM stdin;
\.


--
-- TOC entry 2215 (class 0 OID 32903)
-- Dependencies: 198
-- Data for Name: notebook; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY notebook (notebookid, author, title, creationdate) FROM stdin;
\.


--
-- TOC entry 2211 (class 0 OID 32845)
-- Dependencies: 194
-- Data for Name: notechange; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY notechange (note, person, pointintime) FROM stdin;
\.


--
-- TOC entry 2212 (class 0 OID 32864)
-- Dependencies: 195
-- Data for Name: notexpersongroup; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY notexpersongroup (persongroup, contentid, isnote, cansee, canedit) FROM stdin;
\.


--
-- TOC entry 2213 (class 0 OID 32870)
-- Dependencies: 196
-- Data for Name: notextag; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY notextag (tag, contentid, isnote) FROM stdin;
\.


--
-- TOC entry 2207 (class 0 OID 32820)
-- Dependencies: 190
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY person (personid, email, title, firstname, lastname, birthday, creationdate) FROM stdin;
\.


--
-- TOC entry 2209 (class 0 OID 32831)
-- Dependencies: 192
-- Data for Name: persongroup; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY persongroup (persongroupid, title) FROM stdin;
\.


--
-- TOC entry 2210 (class 0 OID 32840)
-- Dependencies: 193
-- Data for Name: personxpersongroup; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY personxpersongroup (persongroup, person) FROM stdin;
\.


--
-- TOC entry 2205 (class 0 OID 32809)
-- Dependencies: 188
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY tag (tagid, tag) FROM stdin;
\.


--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 185
-- Name: note_noteid_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('note_noteid_seq', 1, false);


--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 197
-- Name: notebook_notebookid_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('notebook_notebookid_seq', 1, false);


--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 189
-- Name: person_personid_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('person_personid_seq', 1, false);


--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 191
-- Name: persongroup_persongroupid_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('persongroup_persongroupid_seq', 1, false);


--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 187
-- Name: tag_tagid_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('tag_tagid_seq', 1, false);


--
-- TOC entry 2055 (class 2606 OID 32783)
-- Name: note note_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY note
    ADD CONSTRAINT note_pkey PRIMARY KEY (noteid);


--
-- TOC entry 2071 (class 2606 OID 32912)
-- Name: notebook notebook_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notebook
    ADD CONSTRAINT notebook_pkey PRIMARY KEY (notebookid);


--
-- TOC entry 2065 (class 2606 OID 32850)
-- Name: notechange notechange_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notechange
    ADD CONSTRAINT notechange_pkey PRIMARY KEY (note, person, pointintime);


--
-- TOC entry 2067 (class 2606 OID 32869)
-- Name: notexpersongroup notexpersongroup_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notexpersongroup
    ADD CONSTRAINT notexpersongroup_pkey PRIMARY KEY (persongroup, contentid, isnote);


--
-- TOC entry 2069 (class 2606 OID 32874)
-- Name: notextag notextag_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notextag
    ADD CONSTRAINT notextag_pkey PRIMARY KEY (tag, contentid, isnote);


--
-- TOC entry 2059 (class 2606 OID 32828)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (personid);


--
-- TOC entry 2061 (class 2606 OID 32839)
-- Name: persongroup persongroup_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY persongroup
    ADD CONSTRAINT persongroup_pkey PRIMARY KEY (persongroupid);


--
-- TOC entry 2063 (class 2606 OID 32844)
-- Name: personxpersongroup personxpersongroup_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY personxpersongroup
    ADD CONSTRAINT personxpersongroup_pkey PRIMARY KEY (persongroup, person);


--
-- TOC entry 2057 (class 2606 OID 32817)
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tagid);


--
-- TOC entry 2072 (class 2606 OID 32914)
-- Name: note note_notebook_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY note
    ADD CONSTRAINT note_notebook_fkey FOREIGN KEY (notebook) REFERENCES notebook(notebookid);


--
-- TOC entry 2084 (class 2606 OID 32919)
-- Name: notebook notebook_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notebook
    ADD CONSTRAINT notebook_author_fkey FOREIGN KEY (author) REFERENCES person(personid);


--
-- TOC entry 2075 (class 2606 OID 32949)
-- Name: notechange notechange_note_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notechange
    ADD CONSTRAINT notechange_note_fkey FOREIGN KEY (note) REFERENCES note(noteid);


--
-- TOC entry 2076 (class 2606 OID 32954)
-- Name: notechange notechange_note_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notechange
    ADD CONSTRAINT notechange_note_fkey1 FOREIGN KEY (note) REFERENCES note(noteid);


--
-- TOC entry 2077 (class 2606 OID 32959)
-- Name: notechange notechange_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notechange
    ADD CONSTRAINT notechange_person_fkey FOREIGN KEY (person) REFERENCES person(personid);


--
-- TOC entry 2079 (class 2606 OID 32939)
-- Name: notexpersongroup notexpersongroup_contentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notexpersongroup
    ADD CONSTRAINT notexpersongroup_contentid_fkey FOREIGN KEY (contentid) REFERENCES note(noteid);


--
-- TOC entry 2080 (class 2606 OID 32944)
-- Name: notexpersongroup notexpersongroup_contentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notexpersongroup
    ADD CONSTRAINT notexpersongroup_contentid_fkey1 FOREIGN KEY (contentid) REFERENCES notebook(notebookid);


--
-- TOC entry 2078 (class 2606 OID 32934)
-- Name: notexpersongroup notexpersongroup_persongroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notexpersongroup
    ADD CONSTRAINT notexpersongroup_persongroup_fkey FOREIGN KEY (persongroup) REFERENCES persongroup(persongroupid);


--
-- TOC entry 2082 (class 2606 OID 32969)
-- Name: notextag notextag_contentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notextag
    ADD CONSTRAINT notextag_contentid_fkey FOREIGN KEY (contentid) REFERENCES note(noteid);


--
-- TOC entry 2083 (class 2606 OID 32974)
-- Name: notextag notextag_contentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notextag
    ADD CONSTRAINT notextag_contentid_fkey1 FOREIGN KEY (contentid) REFERENCES notebook(notebookid);


--
-- TOC entry 2081 (class 2606 OID 32964)
-- Name: notextag notextag_tag_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY notextag
    ADD CONSTRAINT notextag_tag_fkey FOREIGN KEY (tag) REFERENCES tag(tagid);


--
-- TOC entry 2073 (class 2606 OID 32924)
-- Name: personxpersongroup personxpersongroup_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY personxpersongroup
    ADD CONSTRAINT personxpersongroup_person_fkey FOREIGN KEY (person) REFERENCES person(personid);


--
-- TOC entry 2074 (class 2606 OID 32929)
-- Name: personxpersongroup personxpersongroup_persongroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY personxpersongroup
    ADD CONSTRAINT personxpersongroup_persongroup_fkey FOREIGN KEY (persongroup) REFERENCES persongroup(persongroupid);


-- Completed on 2018-01-21 13:42:41

--
-- PostgreSQL database dump complete
--

