package control;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.converter.IntegerStringConverter;
import model.DBManager;
import model.Employee;

/**
 * 
 * @author David Meyer
 */
public class Controller extends Application {

	/* GENERELL */
	@FXML
	AnchorPane guiOuter;
	@FXML
	Pane contentPane;
	
	@FXML
	private Menu menuCreate;
	@FXML
	private Menu menuRead;
	@FXML
	private Menu menuInfo;
	Label menuLabelRead;
	Label menuLabelCreate;
	Label menuLabelInfo;
	
	/* CREATE Seite */
	@FXML
	AnchorPane guiCreate;
	@FXML
	TextField tfID;
	@FXML
	TextField tfFirst;
	@FXML
	TextField tfLast;
	@FXML
	TextField tfAge;
	@FXML
	Button btnAdden;
	
	// Read
	@FXML
	AnchorPane guiRead;
	@FXML
	TableView<Employee> tvTabelle;
	@FXML
	RadioButton radio1, radio2, radio3, radio4, radio5;
	RadioButton[] radioBtns;
	@FXML
	Button btnDelete;
	
	// Info
	@FXML
	AnchorPane guiInfo;
	@FXML
	TextField tfHost;
	@FXML
	TextField tfUser;
	@FXML
	PasswordField tfPassword;
	@FXML
	TextField tfDBName;
	@FXML
	Button btnConnTest;
	@FXML
	Button btnTableCreate;
	
	public enum Scenes {
		CREATE,
		READ,
		INFO
	};
	
	Scenes scenes;
	
	public static void main(String[] args) {
		launch(args);
		DBManager.getInstance();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		loadFXML();
		
		Scene scene = new Scene(guiOuter);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Employee Manager");
		
		setScene(Scenes.READ);
		
		primaryStage.initStyle(StageStyle.TRANSPARENT);

		setDragListeners(primaryStage, scene.getRoot().getChildrenUnmodifiable().get(1));
		primaryStage.show();
		
		//
		testConnection();
	}
	
	/**
	 * Aktiviert Windows-Dragging auf dem angegebenen Element 
	 */
	
	private double dragXOffset;
	private double dragYOffset;
	public void setDragListeners(Stage stage, Node node) {
	
		System.out.println("Drag Events zum Verschieben des Fensters hoeren auf \n"+node);
		node.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				dragXOffset = e.getScreenX() - stage.getX();
				dragYOffset = e.getScreenY() - stage.getY();
			}
		});
		
		node.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				
				stage.setX(e.getScreenX() - dragXOffset);
				stage.setY(e.getScreenY() - dragYOffset);
			}
		});
	}
	
	
	//@SuppressWarnings("unchecked")	// Annotation damit keine Warnings beim Table-Casten vorkommen
	private void loadFXML() {
		try {
			// Generell
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/EmployeeManagerOuter.fxml"));
			guiOuter = loader.load();
			contentPane = (Pane) guiOuter.getChildren().get(2);
			
				// Menue
				// Menu Element unterstuetzt keine onMouseClicked Listener, sondern nur MenuItems innerhalb von Menu Elementen (Dropdown Menue)
				// Workaround: MouseClicked Listener auf ein Label hoeren lassen und das Label als Graphic von dem Menu Element verwenden
			
					// Create
			menuCreate = ((MenuBar) guiOuter.getChildren().get(0)).getMenus().get(0);
	
			menuLabelCreate = new Label(menuCreate.getText());
			menuCreate.setText("");
			menuLabelCreate.setOnMouseClicked(e -> {
				System.out.println("Create Scene wird geladen");
				setScene(Scenes.CREATE);
			});
			menuCreate.setGraphic(menuLabelCreate);
			
					// Read
			menuRead = ((MenuBar) guiOuter.getChildren().get(0)).getMenus().get(1);
			
			menuLabelRead = new Label(menuRead.getText()); // Label nimmt den gleichen Text an wie das Menu welches in der FXML Datei designed wurde
			menuRead.setText(""); // Dem Menu aus der FXML Datei den Text entfernen, da er sonst 2 Mal angezeigt wird
			// MouseClicked Listener mit Lambda Schreibweise:
			menuLabelRead.setOnMouseClicked(e -> {
				System.out.println("Read Scene wird geladen");
				setScene(Scenes.READ);
			});
			// Ohne Lambda Schreibweise:
			/*menuLabelRead.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {
					System.out.println("Read Scene wird geladen");
					setScene(Scenes.READ);
				}
			});*/
			menuRead.setGraphic(menuLabelRead); // Das Label als Graphic fuer das Menu verwenden
			
					// Info
			menuInfo = ((MenuBar) guiOuter.getChildren().get(0)).getMenus().get(2);
			//System.out.println(menuInfo.getText());
	
			menuLabelInfo = new Label(menuInfo.getText());
			menuInfo.setText("");
			menuLabelInfo.setOnMouseClicked(e -> {
				System.out.println("Info Scene wird geladen");
				setScene(Scenes.INFO);
			});
			menuInfo.setGraphic(menuLabelInfo);

			// Create
			loader = new FXMLLoader(); // Fuer jede FXML Datei muss eine neue FXMLLoader Instanz erzeugt werden (ka why)
			loader.setLocation(Class.class.getResource("/view/fxml/Create.fxml"));
			guiCreate = loader.load();
			
			tfID = (TextField) guiCreate.lookup("#tfID");
			tfFirst = (TextField) guiCreate.lookup("#tfFirst");
			tfLast = (TextField) guiCreate.lookup("#tfLast");
			tfAge = (TextField) guiCreate.lookup("#tfAge");
			btnAdden = (Button) guiCreate.lookup("#btnAdden");
			
			btnAdden.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					if(tfAge.getText() == null || tfAge.getText().equals("")) {
						DBManager.getInstance().createEmployee(Integer.parseInt(tfID.getText()), tfFirst.getText(), tfLast.getText());
					}
					else {
						DBManager.getInstance().createEmployee(Integer.parseInt(tfID.getText()), tfFirst.getText(), tfLast.getText(), Integer.parseInt(tfAge.getText()));
					}
					tfID.setText(DBManager.getInstance().getNextId() + "");
				}
			});
			
			// Nur Zahlen einschreibbar
			tfAge.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> arg0, String alt, String neu) {
					if(!neu.matches("\\d{0,7}") && (!neu.equals(""))) {
						tfAge.setText(alt);
					}
				}
			});
			
			// Read
			loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/Read.fxml"));
			guiRead = loader.load();
			
			btnDelete = (Button) guiRead.lookup("#btnDelete");
			btnDelete.setOnAction(e -> {
				deleteEmployee();
			});
			
			// Tabelle der Read-GUI holen
			tvTabelle = (TableView<Employee>) guiRead.lookup("#tvTabelle");
			
			// Einstellungen der Tabelle fuer Employees
			// Vorname-Spalte
			// Property waehlen, die in der jeweiligen Spalte der Tabelle angezeigt werden sollte
			// Property wird auf dem Employee Objekt mittels der get-Methoden erhalten
			// Die Employee Objekte werden der tvTabelle mittels der setItems() Methode beim Laden der Employees gesetzt
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<Employee, String>("first"));
			// Spalte editierbar machen
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setEditable(true);
			// Wenn versucht wird, die Zelle zu aendern, die Zelle mit einem TextFeld ersetzen
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setCellFactory(TextFieldTableCell.forTableColumn());

			// Die selben Sinstellungen fuer die anderen Spalten vornehmen (alle ausser Id, da Ids nicht bearbeitbar sein sollen)
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<Employee, String>("last"));
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setEditable(true);
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setCellFactory(TextFieldTableCell.forTableColumn());
			
			// In der Alter-Spalte muss der Typ auf Integer geandert werden
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<Employee, Integer>("age"));
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setEditable(true);
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
			
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<Employee, Integer>("id"));
		
			tvTabelle.setEditable(true);
			
			// Info
			loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/Info.fxml"));
			guiInfo = loader.load();
			
			radio1 = (RadioButton) guiInfo.lookup("#radio1");
			radio2 = (RadioButton) guiInfo.lookup("#radio2");
			radio3 = (RadioButton) guiInfo.lookup("#radio3");
			radio4 = (RadioButton) guiInfo.lookup("#radio4");
			radio5 = (RadioButton) guiInfo.lookup("#radio5");
			radioBtns = new RadioButton[] {radio1, radio2, radio3, radio4, radio5};
			btnTableCreate = (Button) guiInfo.lookup("#btnTableCreate");
			
			tfHost = (TextField) guiInfo.lookup("#tfHost");
			tfUser = (TextField) guiInfo.lookup("#tfUser");
			tfPassword = (PasswordField) guiInfo.lookup("#tfPassword");
			tfDBName = (TextField) guiInfo.lookup("#tfDBName");
			tfHost.setText(DBManager.getInstance().getDb_host());
			tfUser.setText(DBManager.getInstance().getUser());
			tfPassword.setText(DBManager.getInstance().getPass());
			tfDBName.setText(DBManager.getInstance().getDb_name());
			
			tfHost.setOnKeyReleased(e -> {
				System.out.println(tfHost.getText());
				DBManager.getInstance().setDb_host(tfHost.getText());
			});
			tfUser.setOnKeyReleased(e -> {
				DBManager.getInstance().setUser(tfUser.getText());
			});
			tfPassword.setOnKeyReleased(e -> {
				DBManager.getInstance().setPass(tfPassword.getText());
			});
			tfDBName.setOnKeyReleased(e -> {
				DBManager.getInstance().setDb_name(tfDBName.getText());
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getFirst() {
		return null;
	}
	
	public void setScene(Scenes scenes) {
		
		this.scenes = scenes;
		switch(scenes) {
			case CREATE: loadScene(guiCreate);
				tfID.setText(DBManager.getInstance().getNextId() + "");
				tfFirst.setText("");
				tfLast.setText("");
				tfAge.setText("");
				break;
			case READ: loadScene(guiRead); break;
			case INFO: loadScene(guiInfo); break;
		}
	}
	
	/**
	 * Ladet die angegebene Pane auf die Content Pane der Stage
	 * Der Header und die Menu Bar bleiben auf der Stage vorhanden, nur die contentPane wird geaendert
	 * @param pane AnchorPane mit dem Content der im Content Bereich der Stage angezeigt werden soll
	 */
	public void loadScene(AnchorPane pane) {
		
		contentPane.getChildren().clear();
		contentPane.getChildren().add(pane);
	}
	
	@FXML
	/**
	 * Employees werden aus der Datenbank geladen und in die Read-Tabelle eingetragen
	 */
	public void loadEmployees() {
		
		System.out.println("Employees werden geladen...");
		//^orPopup e = new ErrorPopup("sasdd");
		//e.init();
		ObservableList<Employee> employees = FXCollections.observableArrayList(DBManager.getInstance().getEmployees());
		tvTabelle.setItems(employees);
		

		System.out.println("Employees wurden geladen");
		if(employees.isEmpty()) System.out.println("Es liegen keine Employees vor");
	}
	
	public void deleteEmployee() {
		
		ObservableList<Employee> employees = tvTabelle.getSelectionModel().getSelectedItems();
		ArrayList<Employee> a = new ArrayList<Employee>();
		a.addAll(employees);
		DBManager.getInstance().deleteEmployee(a);
		loadEmployees();
	}
	
	@FXML
	public void menueClickCreate() {
		System.out.println("Create Scene geladen");
		setScene(Scenes.CREATE);
	}
	
	@FXML
	public void menueClickRead() {
		System.out.println("Read Scene geladen");
		setScene(Scenes.READ);
	}
	
	@FXML
	/**
	 * Wenn in der Lesen-Tabelle ein Vorname bearbeitet wird,
	 * das Employee Objekt ebenfalls veraendern und
	 * Aenderungen sofort in die Datenbank speichern
	 * @param e Bearbeitungs Event
	 */
	public void changeEmployeeTableFirst(CellEditEvent<Employee, String> e) {
		
		e.getRowValue().setFirst(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
	@FXML
	public void changeEmployeeTableLast(CellEditEvent<Employee, String> e) {
		
		e.getRowValue().setLast(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
	@FXML
	public void changeEmployeeTableAge(CellEditEvent<Employee, Integer> e) {
		
		e.getRowValue().setAge(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
	
	@FXML
	public void onCloseBtnPress() {
		System.exit(0);
	}
	
	@FXML
	/**
	 * F�hrt Verbindungstest aus und gibt Ergebnis an den
	 * Radio Buttons im Info Men� aus
	 */
	public void testConnection() {
		System.out.println("Verbindungstest gestartet...");
		boolean[] ergb = DBManager.getInstance().testConnection();
		radioBtns = new RadioButton[] {radio1, radio2, radio3, radio4, radio5};
		for(int i = 0; i < radioBtns.length; i++) {
			radioBtns[i].setSelected(false); // zuruecksetzen
			radioBtns[i].setSelected(ergb[i]);
		}
		if(ergb[3] && !ergb[4]) { // alle bis auf der letzte test erfolgreich
			btnTableCreate.setDisable(false);
		}
		else if(ergb[4]) {
			btnTableCreate.setDisable(true);
		}
		System.out.println("Verbindungstest beendet");
	}
	
	@FXML
	/**
	 * Erstellt die Employee Table in der Datenbank
	 * und testet danach die Verbindung zu dieser
	 */
	public void tabelleErstellen() {
		
		DBManager.getInstance().createEmployeeTable();
		testConnection();
	}
}