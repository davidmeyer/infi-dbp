package model;

/**
 * Repraesentation einer Zeile in der Datenbank-Tabelle "Employee".
 * Die DB-Spalten sind als Variablen ausgefuehrt. 
 * 
 * @author David Meyer
 */
public class Employee {

	// VARIABLEN
	/** Vorname */
	private String first;
	/** Nachname */
	private String last;
	/** Alter in Jahren (optional) */
	private int age;
	/** Eindeutige Identifikationsnummer */
	private int id;
	
	// KONSTRUKTOREN
	/** Konstruktor
	 * @param id Eindeutige Identifikationsnummer
	 * @param first Vorname
	 * @param last Nachname
	 * @param age Alter in Jahren
	 */
	public Employee(int id, String first, String last, int age) {
		this(id, first, last);
		this.age = age;
	}
	/** Konstruktor
	 * @param id Eindeutige Identifikationsnummer
	 * @param first Vorname
	 * @param last Nachname
	 */
	public Employee(int id, String first, String last) {
		this(first, last);
		this.id = id;
	}
	/** Konstruktor
	 * @param first Vorname
	 * @param last Nachname
	 */
	public Employee(String first, String last) {
		this.first = first;
		this.last = last;
	}
	/** Konstruktor
	 * @param first Vorname
	 * @param last Nachname
	 * @param age Alter in Jahren
	 */
	public Employee(String first, String last, int age) {
		this(first, last);
		this.age = age;
	}

	// GETTER & SETTER
	/**
	 * @return Vorname des Employees
	 */
	public String getFirst() {
		return first;
	}
	/**
	 * @param first Vorname des Employees
	 */
	public void setFirst(String first) {
		if(this.first != null) System.out.println("Vorname von "+this.first+" auf "+first+" ge�ndert");
		this.first = first;
	}
	/**
	 * @return Nachname des Employees
	 */
	public String getLast() {
		return last;
	}
	/**
	 * @param last Nachname des Employees
	 */
	public void setLast(String last) {
		if(this.last != null) System.out.println("Nachname von "+this.last+" auf "+last+" ge�ndert");
		this.last = last;
	}
	/**
	 * @return Vorname und Nachname des Employees
	 */
	public String getFullName(){
		return first+" "+last;
	}
	/**
	 * @return Alter des Employees in Jahren
	 */
	public int getAge() {
		return age;
	}
	/**
	 * (Optional)
	 * Nur positive Werte
	 * @param age Alter des Employees in Jahren
	 */
	public void setAge(int age) {
		if(age >= 0) {
			if(this.age != 0) System.out.println("Alter von "+this.age+" auf "+age+" ge�ndert");
			this.age = age;
		} else 
			System.out.println("FEHLER: Es wurde versucht, das Alter auf einen negativen Wert zu setzen");
	}
	/**
	 * @return Eindeutige Identifikationsnummer des Employees
	 */
	public int getId() {
		return id;
	}
	/**
	 * Nur positive Werte
	 * @param id Eindeutige Identifikationsnummer des Employees
	 */
	public void setId(int id) {
		if(id >= 0)
			this.id = id;
		else 
			System.out.println("FEHLER: Es wurde versucht, die ID auf einen negativen Wert zu setzen");
	}	
	
	@Override
	/**
	 * String-Repraesentation der Variablen des Employees
	 */
	public String toString() {
		return String.format("ID: %d\nVorname: %s\nNachname: %s\nAlter: %d\n", this.getId(), this.getFirst(), this.getLast(), this.getAge());
	}
}