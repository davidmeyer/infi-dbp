package model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Datenbank-Manager (Singleton)
 * zum Aufbau einer Verbindung zu Datenbank
 * f�r einen anschlie�enden Datenaustausch.
 * 
 * Zugangsdaten sind hier gespeichert und koennen via
 * Info-Panel der GUI geandert werden.
 * 
 * Diese Klasse enth�lt vorgefertigte Methoden um
 * Employee-Daten zu
 * C - Erstellen (Create)
 * R - Lesen (Read)
 * U - �ndern (Update)
 * D - L�schen (Delete)
 * 
 * Viele Methoden sind �berladen, um die Eingabe der Daten
 * in verschiedensten Formen (Objekte, Strings, Arrays, ArrayLists, etc.)
 * zu erm�glichen.
 * 
 * @author David Meyer
 */
public class DBManager {

	/** Pfad der Driver Klasse f�r PostgreSQL
	 * (nicht �nderbar) */
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	/** Beginn der URL zum Verbindungsaufbau
	 * (nicht �nderbar)
	 */
	static final String DB_URL = "jdbc:postgresql://";
	
	/** Hostadresse */
	private String db_host = "localhost";
	/** Username des DB-Users */
	private String user = "dbp_user";
	/** Passwort des DB-Users */
	private String pass = "passwort";
	/** Name der Datenbank */
	private String db_name = "dbp";

	private String last_error;
	
	/** Verbindungsinstanz zur DB */
	private static Connection conn;
	
	/** Einzige Instanz des DB Managers
	 * Enthaelt vordefinierte Methoden zum 
	 * Hinzufugen, Lesen, Ver�ndern und L�schen
	 * von Employee Daten und mehr
	 */
	private static DBManager instance;

	private DBManager() {}
	
	/**
	 * Test
	 * @param args keine Laufzeitparameter
	 */
	public static void main(String[] args) {
/*
		try {
			Class.forName(JDBC_DRIVER);
			getInstance().getConnection();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
*/
		//getInstance().createEmployeeTable();
		//getInstance().createTestEmployees();
		//getInstance().getEmployees();	
		
		for(boolean bool : getInstance().testConnection()) {
			System.out.println(bool);
		}
		
		Scanner scan = new Scanner(System.in);
		
		String command;
		while(true) {
			
			command = scan.nextLine();
			System.out.println(command);
			String[] commands = command.split(" ");
			switch(commands[0]) {
				case "delete": getInstance().deleteEmployee(Integer.parseInt(commands[1])); break;
			}
		}
	}

	public static DBManager getInstance() 
	{
		if(instance == null) instance = new DBManager();
		return instance;
	}
	
	// GETTER
	public String getDb_host() {
		return db_host;
	}
	public String getUser() {
		return user;
	}
	public String getPass() {
		return pass;
	}
	public String getDb_name() {
		return db_name;
	}
	// SETTER
	public void setDb_host(String db_host) {
		this.db_host = db_host;
		System.out.println("DB Host wurde auf " + this.db_host + " ge�ndert");
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}
	
	public Connection getConnection() {
		if(conn == null) {
			try {
				Class.forName(JDBC_DRIVER);
				System.out.println(db_host);
				conn = DriverManager.getConnection(DB_URL+db_host+"/"+db_name, user, pass);
			} catch (SQLException e) {
				e.printStackTrace();
				this.last_error = e.getMessage();
				System.out.println(e.getMessage());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				this.last_error = "0";
			}
		}
		return conn;
	}
	
	/**
	 * getConnection:
	 * 0 = JDBC_Driver nicht gefunden
	 * 1 = Konnte Server nicht erreichen
	 * 2 = Konnte nicht einloggen (Benutzername oder Passwort inkorrekt)
	 * 3 = DB nicht gefunden
	 * Daten:
	 * 4 = Tabelle nicht gefunden
	 * 
	 * @return
	 */
	public int getLastErrorNo() {
		System.out.println(last_error);
		if(last_error == null) return -2;
		if(last_error.equals("0") || last_error.startsWith("Connection to") || last_error.contains("Connection error") || last_error.contains("refused")) {
			return 0;
		}
		if(last_error.equals("Der Verbindungsversuch schlug fehl.")) {
			return 1;
		}
		if(last_error.startsWith("FATAL: Passwort-Authentifizierung f�r Benutzer")) {
			return 2;
		}
		if(last_error.startsWith("FATAL: Datenbank ")) {
			return 3;
		}
		if(last_error.startsWith("FEHLER: Relation ")) {
			return 4;
		} 
		return -1;
	}
	
	public String getLastErrorMsg() {
		return last_error;
	}

	//////////////////////////
	/**
	 * 
	 * Arraywerte:
	 * 0 = JDBC_Driver geladen
	 * 1 = Verbindung zum Server aufgebaut
	 * 2 = Eingeloggt
	 * 3 = DB gefunden
	 * 4 = Tabelle erstellt
	 * @return 
	 */
	public boolean[] testConnection() {
		this.last_error = null;
		closeConnection(); // Connection wieder deaktivieren
		try {
			
			if(getConnection() != null) {
				Statement stm = getConnection().createStatement();
				stm.executeQuery("SELECT * FROM Employee LIMIT 1");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			this.last_error = e.getMessage();
		}
		System.out.println("Auswertung des Tests");
		boolean[] bool = new boolean[5];
		int errno = getLastErrorNo();
		if(errno < 0) errno = 5; // Fehler kleiner als 0 bedeutet anderer oder kein Fehler -> alle Arraywerte true schalten
		for(int i = 0; i < 5; i++) {
			if(i < errno) {
				bool[i] = true;
			}
			else {
				bool[i] = false;
			}
		}
		return bool;
	}
	
	/** 
	 * Verbindung zur DB wieder schliessen
	 */
	public void closeConnection() {
		if(conn == null) System.out.println("Die Verbindung zur Datenbank wollte geschlossen werden, obwohl keine Verbindung aufgebaut war");
		else {
			conn = null;
			System.out.println("Die Verbindung zur Datenbank wurde geschlossen");
		}
	}
	
	public int getNextId() {
		try {
			Statement stm = getConnection().createStatement();
			ResultSet rs = stm.executeQuery("SELECT max(ID) FROM Employee");
			rs.next();
			return rs.getInt(1)+1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	//////////////////////////////////////
	
	public void createEmployeeTable() {

		try {
			Statement stm = getConnection().createStatement();

			String sql = "CREATE TABLE IF NOT EXISTS Employee ("
					+ "ID SERIAL PRIMARY KEY, "
					+ "first character varying NOT NULL, "
					+ "last character varying NOT NULL,"
					+ "age int)";

			ResultSet rs = stm.executeQuery(sql);
		} catch (SQLException e) {
			this.last_error = e.getMessage();
			e.printStackTrace();
		}	
	}

	// Read
	public ArrayList<Employee> getEmployees() {

		ArrayList<Employee> employees = new ArrayList<Employee>();
		try {
			Statement stm = getConnection().createStatement();
			ResultSet rs = stm.executeQuery("SELECT Id, first, last, age FROM Employee");

			while(rs.next()) {
				employees.add(new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
				System.out.println(employees.get(employees.size()-1)
						+"---");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employees;
	}

	PreparedStatement pstmReadEmployee;
	public Employee getEmployee(int id) {
		
			try {
				if(pstmReadEmployee == null)
					pstmReadEmployee = getConnection().prepareStatement("SELECT Id, first, last, age FROM Employee WHERE Id = ?");
				pstmReadEmployee.setInt(1, id);
				
				ResultSet rs = pstmReadEmployee.executeQuery();
				Employee e = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
				System.out.println(e);
				return e;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	// Create
	/** Erstellt einen Test-Employee in der Datenbank
	 * @return Employee Objekt
	 */
	public Employee createTestEmployees() {
		Statement stm;
		try {
			stm = getConnection().createStatement();

			stm.executeQuery("INSERT INTO Employee (ID, first, last, age) VALUES ("
					+ "1, 'David', 'Meyer', 19)");
			System.out.println("Ein Test-Employee wurde in der Datenbank eingetragen");
			return new Employee(1, "David", "Meyer", 19);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/** Vorbereitetes Statement zum Hinzufuegen eines Employees in der Datenbank mit allen Properties
	 * Wird in der Methode createEmployee erstellt falls null 
	 */
	PreparedStatement pstmCreateFull;
	/** Vorbereitetes Statement zum Hinzufuegen eines Employees in der Datenbank mit allen Properties ausser dem Alter
	 *  Wird in der Methode createEmployee erstellt falls null 
	 */
	PreparedStatement pstmCreateNoAge;
	/**
	 * Erstellt einen neuen Employee in der Datenbank mit allen Properties
	 * und gibt ein Employee Objekt zurueck
	 * @param id ID 
	 * @param first Vorname
	 * @param last Nachname
	 * @param age Alter in Jahren
	 * @return Employee Objekt
	 */
	public Employee createEmployee(int id, String first, String last, int age) {	
		try {
			if(pstmCreateFull == null)
				pstmCreateFull = getConnection().prepareStatement("INSERT INTO Employee (first, last, age) VALUES (?, ?, ?)");
			
			pstmCreateFull.setString(1, first);
			pstmCreateFull.setString(2, last);
			pstmCreateFull.setInt(3, age);
			pstmCreateFull.execute();
			
			Employee e = new Employee(id, first, last, age);
			System.out.println("Ein neuer Employee wurde in der Datenbank eingetragen:");
			System.out.println(e);
			return new Employee(id, first, last, age);
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Erstellt einen neuen Employee in der Datenbank ohne der Age Property
	 * und gibt ein Employee Objekt zurueck
	 * @param id ID
	 * @param first Vorname
	 * @param last Nachname
	 * @return Employee Objekt
	 */
	public Employee createEmployee(int id, String first, String last) {
		try {
			if(pstmCreateNoAge == null)
				pstmCreateNoAge = getConnection().prepareStatement("INSERT INTO Employee (first, last) VALUES (?, ?)");
			
			pstmCreateNoAge.setString(1, first);
			pstmCreateNoAge.setString(2, last);
			pstmCreateNoAge.executeQuery();
			
			return new Employee(id, first, last);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Erstellt mehrere Employees in der Datenbank aus einer ArrayList von Employee Objekten
	 * @param employees ArrayList aus Employees
	 */
	public void createEmployee(ArrayList<Employee> employees) {
		createEmployee((Employee[]) employees.toArray());
	}
	/**
	 * Erstellt mehrere Employees in der Datenbank aus einem Array von Employee Objekten
	 * @param employees Array aus Employees
	 */
	public void createEmployee(Employee[] employees) {
		for(Employee e : employees) {
			createEmployee(e.getId(), e.getFirst(), e.getLast(), e.getAge());
		}
	}
	
	// Deletes
	/** Vorbereitetes Statement zum Loeschen eines Employees mittels seiner ID an Index 1
	 * Wird in der Methode deleteEmployee erstellt falls null
	 */
	private PreparedStatement pstmDelete;
	/**
	 * Loescht einen Employee mittels der ID
	 * @param id ID des Employees
	 */
	public void deleteEmployee(int id) {
		
		try {
			if(pstmDelete == null)
				pstmDelete = getConnection().prepareStatement("DELETE from Employee WHERE ID = ?");
			pstmDelete.setInt(1, id);
			pstmDelete.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Der Employee mit der ID " + id + " konnte nicht geloescht werden");
			return;
		}
		System.out.println("Der Employee mit der ID " + id + " wurde geloescht");
	}
	/**
	 * Loescht einen Employee mittels dem Employee Objekt
	 * @param e Objekt des Employees
	 */
	public void deleteEmployee(Employee e) {
		deleteEmployee(e.getId());
	}
	/**
	 * Loescht mehrere Employees mittels einem Array aus Employee Objekten
	 * @param employees Array aus Employee Objekten
	 */
	public void deleteEmployee(Employee[] employees) {
		for(Employee e : employees) {
			deleteEmployee(e.getId());
		}
	}
	/**
	 * Loescht mehrere Employees mittels einer ArrayList aus Employee Objekten
	 * @param employees ArrayList aus Employee Objekten
	 */
	public void deleteEmployee(ArrayList<Employee> employees) {
		for(Employee e : employees) {
			deleteEmployee(e.getId());
		}
	}
	/**
	 * Loescht mehrere Employees mittels einem Array aus IDs
	 * @param ids Array mit Employee IDs
	 */
	public void deleteEmployees(Integer[] ids) {
		for(int i = 0; i < ids.length; i++) {
			deleteEmployee(ids[i]);
		}
	}	
	/**
	 * Loescht mehrere Employees mittels einer ArrayList aus IDS
	 * @param ids ArrayList mit IDs
	 */
	public void deleteEmployees(ArrayList<Integer> ids) {
		deleteEmployees((Integer[]) ids.toArray());
	}
	
	// Update
	PreparedStatement pstmUpdateFull;
	PreparedStatement pstmUpdateNoAge;
	public Employee updateEmployee(int id, String first, String last, int age) {
			try {
				if(pstmUpdateFull == null)
					pstmUpdateFull = getConnection().prepareStatement("UPDATE employee SET first = ?, last = ?, age = ? WHERE Id = ?");
				
				pstmUpdateFull.setString(1, first);
				pstmUpdateFull.setString(2, last);
				pstmUpdateFull.setInt(3, age);
				pstmUpdateFull.setInt(4, id);
				pstmUpdateFull.execute();
				
				return new Employee(id, first, last, age);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	public Employee updateEmployee(int id, String first, String last) {
			try {
				if(pstmUpdateNoAge == null)
					getConnection().prepareStatement("UPDATE employee SET first = ?, last = ? WHERE Id = ?");
				
				pstmUpdateNoAge.setString(1, first);
				pstmUpdateNoAge.setString(2, last);
				pstmUpdateNoAge.setInt(3, id);
				pstmUpdateNoAge.execute();
				
				return new Employee(id, first, last);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	public void updateEmployee(Employee employee) {
		updateEmployee(employee.getId(), employee.getFirst(), employee.getLast(), employee.getAge());
	}
	
	public void updateEmployee(ArrayList<Employee> employees) {
		updateEmployee((Employee[]) employees.toArray());
	}
	
	public void updateEmployee(Employee[] employees) {
		for(Employee e : employees) {
			updateEmployee(e);
		}
	}


	// Employees: CRUD - Create Read Update Delete  
}
