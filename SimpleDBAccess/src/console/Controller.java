package console;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.SynchronousQueue;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import model.DBManager;
import model.Employee;

public class Controller extends Application {

	/* GENERELL */
	@FXML
	AnchorPane guiOuter;
	@FXML
	Pane contentPane;
	
	/* CREATE Seite */
	@FXML
	AnchorPane guiCreate;
	@FXML
	TextField tfID;
	@FXML
	TextField tfFirst;
	@FXML
	TextField tfLast;
	@FXML
	TextField tfAge;
	@FXML
	Button btnAdden;
	
	// Read
	@FXML
	AnchorPane guiRead;
	@FXML
	TableView<Employee> tvTabelle;

	public enum Scenes {
		CREATE,
		READ
	};
	
	Scenes scenes;
	
	public static void main(String[] args) {
		launch(args);
		DBManager.getInstance();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		loadFXML();
		
		Scene scene = new Scene(guiOuter);
		primaryStage.setScene(scene);
		primaryStage.setTitle("");
		
		setScene(Scenes.READ);
		
		
		
		//liste.add("eee");

		
		primaryStage.show();
	}
	
	//@SuppressWarnings("unchecked")	// Annotation damit keine Warnings beim Table-Casten vorkommen
	private void loadFXML() {
		try {
			// Generell
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/EmployeeManagerOuter.fxml"));
			guiOuter = loader.load();
			contentPane = (Pane) guiOuter.getChildren().get(2);
			
			// Create
			loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/Create.fxml"));
			guiCreate = loader.load();
			
			// Read
			loader = new FXMLLoader();
			loader.setLocation(Class.class.getResource("/view/fxml/Read.fxml"));
			guiRead = loader.load();
			
			// Tabelle der Read-GUI holen
			tvTabelle = (TableView<Employee>) guiRead.lookup("#tvTabelle");
			
			// Einstellungen der Tabelle fuer Employees
			// Vorname-Spalte
			// Property waehlen, die in der jeweiligen Spalte der Tabelle angezeigt werden sollte
			// Property wird auf dem Employee Objekt mittels der get-Methoden erhalten
			// Die Employee Objekte werden der tvTabelle mittels der setItems() Methode beim Laden der Employees gesetzt
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<Employee, String>("first"));
			// Spalte editierbar machen
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setEditable(true);
			// Wenn versucht wird, die Zelle zu aendern, die Zelle mit einem TextFeld ersetzen
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(1)).setCellFactory(TextFieldTableCell.forTableColumn());

			// Die selben Sinstellungen fuer die anderen Spalten vornehmen (alle ausser Id, da Ids nicht bearbeitbar sein sollen)
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<Employee, String>("last"));
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setEditable(true);
			((TableColumn<Employee,String>) tvTabelle.getColumns().get(2)).setCellFactory(TextFieldTableCell.forTableColumn());
			
			// In der Alter-Spalte muss der Typ auf Integer geandert werden
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<Employee, Integer>("age"));
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setEditable(true);
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
			
			((TableColumn<Employee,Integer>) tvTabelle.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<Employee, Integer>("id"));

			tvTabelle.setEditable(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getFirst() {
		return null;
	}
	
	public void setScene(Scenes scenes) {
		
		this.scenes = scenes;
		switch(scenes) {
			case CREATE: loadScene(guiCreate); break;
			case READ: loadScene(guiRead); break;
		}
	}
	
	/**
	 * Ladet die angegebene Pane auf die Content Pane der Stage
	 * Der Header und die Menu Bar bleiben auf der Stage vorhanden, nur die contentPane wird geaendert
	 * @param pane AnchorPane mit dem Content der im Content Bereich der Stage angezeigt werden soll
	 */
	public void loadScene(AnchorPane pane) {
		
		contentPane.getChildren().clear();
		contentPane.getChildren().add(pane);
	}
	
	@FXML
	/**
	 * Employees werden aus der Datenbank geladen und in die Read-Tabelle eingetragen
	 */
	public void loadEmployees() {
		
		ObservableList<Employee> employees = FXCollections.observableArrayList(DBManager.getInstance().getEmployees());
		tvTabelle.setItems(employees);
	}
	
	@FXML
	/**
	 * Wenn in der Lesen-Tabelle ein Vorname bearbeitet wird,
	 * das Employee Objekt ebenfalls veraendern und
	 * Aenderungen sofort in die Datenbank speichern
	 * @param e Bearbeitungs Event
	 */
	public void changeEmployeeTableFirst(CellEditEvent<Employee, String> e) {
		
		e.getRowValue().setFirst(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
	
	public void changeEmployeeTableLast(CellEditEvent<Employee, String> e) {
		
		e.getRowValue().setLast(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
	
	public void changeEmployeeTableAge(CellEditEvent<Employee, Integer> e) {
		
		e.getRowValue().setAge(e.getNewValue());
		DBManager.getInstance().updateEmployee(e.getRowValue());
	}
}