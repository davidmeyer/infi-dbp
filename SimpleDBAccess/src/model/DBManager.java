package model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class DBManager {

	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/dbp";

	static final String USER = "dbp_user";
	static final String PASS = "passwort";

	private static Connection conn;

	private static DBManager instance;

	private DBManager() {}

	public static void main(String[] args) {

		try {
			Class.forName(JDBC_DRIVER);
			getInstance().getConnection();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		//getInstance().createEmployeeTable();
		//getInstance().createTestEmployees();
		getInstance().getEmployees();	
		
		Scanner scan = new Scanner(System.in);
		
		String command;
		while(true) {
			
			command = scan.nextLine();
			System.out.println(command);
			String[] commands = command.split(" ");
			switch(commands[0]) {
				case "delete": getInstance().deleteEmployee(Integer.parseInt(commands[1])); break;
			}
		}
	}

	public static DBManager getInstance() 
	{
		if(instance == null) instance = new DBManager();
		return instance;
	}

	public Connection getConnection() {
		if(conn == null) {
			try {
				conn = DriverManager.getConnection(DB_URL, USER, PASS);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conn;
	}

	//////////////////////////
	
	public void createEmployeeTable() {

		try {
			Statement stm = getConnection().createStatement();

			String sql = "CREATE TABLE IF NOT EXISTS Employee ("
					+ "ID SERIAL PRIMARY KEY, "
					+ "first character varying NOT NULL, "
					+ "last character varying NOT NULL,"
					+ "age int)";

			ResultSet rs = stm.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	// Read
	public ArrayList<Employee> getEmployees() {

		ArrayList<Employee> employees = new ArrayList<Employee>();
		try {
			Statement stm = getConnection().createStatement();
			ResultSet rs = stm.executeQuery("SELECT Id, first, last, age FROM Employee");

			while(rs.next()) {
				employees.add(new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
				System.out.println(employees.get(employees.size()-1)
						+"---");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employees;
	}

	PreparedStatement pstmReadEmployee;
	public Employee getEmployee(int id) {
		
			try {
				if(pstmReadEmployee == null)
					pstmReadEmployee = getConnection().prepareStatement("SELECT Id, first, last, age FROM Employee WHERE Id = ?");
				pstmReadEmployee.setInt(1, id);
				
				ResultSet rs = pstmReadEmployee.executeQuery();
				Employee e = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
				System.out.println(e);
				return e;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	// Create
	/** Erstellt einen Test-Employee in der Datenbank
	 * @return Employee Objekt
	 */
	public Employee createTestEmployees() {
		Statement stm;
		try {
			stm = getConnection().createStatement();

			stm.executeQuery("INSERT INTO Employee (ID, first, last, age) VALUES ("
					+ "1, 'David', 'Meyer', 19)");
			System.out.println("Ein Test-Employee wurde in der Datenbank eingetragen");
			return new Employee(1, "David", "Meyer", 19);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/** Vorbereitetes Statement zum Hinzufuegen eines Employees in der Datenbank mit allen Properties
	 * Wird in der Methode createEmployee erstellt falls null 
	 */
	PreparedStatement pstmCreateFull;
	/** Vorbereitetes Statement zum Hinzufuegen eines Employees in der Datenbank mit allen Properties ausser dem Alter
	 *  Wird in der Methode createEmployee erstellt falls null 
	 */
	PreparedStatement pstmCreateNoAge;
	/**
	 * Erstellt einen neuen Employee in der Datenbank mit allen Properties
	 * und gibt ein Employee Objekt zurueck
	 * @param id ID 
	 * @param first Vorname
	 * @param last Nachname
	 * @param age Alter in Jahren
	 * @return Employee Objekt
	 */
	public Employee createEmployee(int id, String first, String last, int age) {	
		try {
			if(pstmCreateFull == null)
				pstmCreateFull = getConnection().prepareStatement("INSERT INTO Employee (ID, first, last, age) VALUES (?, '?', '?', ?)");
			
			pstmCreateFull.setInt(1, id);
			pstmCreateFull.setString(2, first);
			pstmCreateFull.setString(3, last);
			pstmCreateFull.setInt(4, age);
			pstmCreateFull.executeQuery();
			
			Employee e = new Employee(id, first, last, age);
			System.out.println("Ein neuer Employee wurde in der Datenbank eingetragen:");
			System.out.println(e);
			return new Employee(id, first, last, age);
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Erstellt einen neuen Employee in der Datenbank ohne der Age Property
	 * und gibt ein Employee Objekt zurueck
	 * @param id ID
	 * @param first Vorname
	 * @param last Nachname
	 * @return Employee Objekt
	 */
	public Employee createEmployee(int id, String first, String last) {
		try {
			if(pstmCreateNoAge == null)
				pstmCreateNoAge = getConnection().prepareStatement("INSERT INTO Employee (ID, first, last) VALUES (?, '?', '?')");
			
			pstmCreateNoAge.setInt(1, id);
			pstmCreateNoAge.setString(2, first);
			pstmCreateNoAge.setString(3, last);
			pstmCreateNoAge.executeQuery();
			
			return new Employee(id, first, last);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Erstellt mehrere Employees in der Datenbank aus einer ArrayList von Employee Objekten
	 * @param employees ArrayList aus Employees
	 */
	public void createEmployee(ArrayList<Employee> employees) {
		createEmployee((Employee[]) employees.toArray());
	}
	/**
	 * Erstellt mehrere Employees in der Datenbank aus einem Array von Employee Objekten
	 * @param employees Array aus Employees
	 */
	public void createEmployee(Employee[] employees) {
		for(Employee e : employees) {
			createEmployee(e.getId(), e.getFirst(), e.getLast(), e.getAge());
		}
	}
	
	// Deletes
	/** Vorbereitetes Statement zum Loeschen eines Employees mittels seiner ID an Index 1
	 * Wird in der Methode deleteEmployee erstellt falls null
	 */
	private PreparedStatement pstmDelete;
	/**
	 * Loescht einen Employee mittels der ID
	 * @param id ID des Employees
	 */
	public void deleteEmployee(int id) {
		
		try {
			if(pstmDelete == null)
				pstmDelete = getConnection().prepareStatement("DELETE FROM Employee WHERE ID = ?");
			pstmDelete.setInt(1, id);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Der Employee mit der ID " + id + " konnte nicht geloescht werden");
			return;
		}
		System.out.println("Der Employee mit der ID " + id + " wurde geloescht");
	}
	/**
	 * Loescht einen Employee mittels dem Employee Objekt
	 * @param e Objekt des Employees
	 */
	public void deleteEmployee(Employee e) {
		deleteEmployee(e.getId());
	}
	/**
	 * Loescht mehrere Employees mittels einem Array aus Employee Objekten
	 * @param employees Array aus Employee Objekten
	 */
	public void deleteEmployee(Employee[] employees) {
		for(Employee e : employees) {
			deleteEmployee(e.getId());
		}
	}
	/**
	 * Loescht mehrere Employees mittels einer ArrayList aus Employee Objekten
	 * @param employees ArrayList aus Employee Objekten
	 */
	public void deleteEmployee(ArrayList<Employee> employees) {
		deleteEmployee((Employee[]) employees.toArray());
	}
	/**
	 * Loescht mehrere Employees mittels einem Array aus IDs
	 * @param ids Array mit Employee IDs
	 */
	public void deleteEmployees(Integer[] ids) {
		for(int i = 0; i < ids.length; i++) {
			deleteEmployee(ids[i]);
		}
	}	
	/**
	 * Loescht mehrere Employees mittels einer ArrayList aus IDS
	 * @param ids ArrayList mit IDs
	 */
	public void deleteEmployees(ArrayList<Integer> ids) {
		deleteEmployees((Integer[]) ids.toArray());
	}
	
	// Update
	PreparedStatement pstmUpdateFull;
	PreparedStatement pstmUpdateNoAge;
	public Employee updateEmployee(int id, String first, String last, int age) {
			try {
				if(pstmUpdateFull == null)
					pstmUpdateFull = getConnection().prepareStatement("UPDATE employee SET first = ?, last = ?, age = ? WHERE Id = ?");
				
				pstmUpdateFull.setString(1, first);
				pstmUpdateFull.setString(2, last);
				pstmUpdateFull.setInt(3, age);
				pstmUpdateFull.setInt(4, id);
				pstmUpdateFull.execute();
				
				return new Employee(id, first, last, age);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	public Employee updateEmployee(int id, String first, String last) {
			try {
				if(pstmUpdateNoAge == null)
					getConnection().prepareStatement("UPDATE employee SET first = ?, last = ? WHERE Id = ?");
				
				pstmUpdateNoAge.setString(1, first);
				pstmUpdateNoAge.setString(2, last);
				pstmUpdateNoAge.setInt(3, id);
				pstmUpdateNoAge.execute();
				
				return new Employee(id, first, last);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
	}
	
	public void updateEmployee(Employee employee) {
		updateEmployee(employee.getId(), employee.getFirst(), employee.getLast(), employee.getAge());
	}
	
	public void updateEmployee(ArrayList<Employee> employees) {
		updateEmployee((Employee[]) employees.toArray());
	}
	
	public void updateEmployee(Employee[] employees) {
		for(Employee e : employees) {
			updateEmployee(e);
		}
	}


	// Employees: CRUD - Create Read Update Delete  
}
