package model;

import java.util.concurrent.SynchronousQueue;

import javafx.beans.property.SimpleStringProperty;

public class Employee {

	/** Vorname */
	private SimpleStringProperty first;
	/** Nachname */
	private String last;
	/** Alter in Jahren (optional) */
	private int age;
	/** Eindeutige Identifikationsnummer */
	private int id;
	
	public Employee(int id, String first, String last, int age) {
		
		this.first = new SimpleStringProperty(first);
		this.last = last;
		this.age = age;
		this.id = id;
	}
	
	public Employee(int id, String first, String last) {
		
		this.first = new SimpleStringProperty(first);
		this.last = last;
		this.id = id;
	}

	/**
	 * @return Vorname des Employees
	 */
	public String getFirst() {
		return first.get();
	}

	/**
	 * @param first Vorname des Employees
	 */
	public void setFirst(String first) {
		this.first = new SimpleStringProperty(first);
	}

	/**
	 * @return Nachname des Employees
	 */
	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	/**
	 * @return Alter in Jahren
	 */
	public int getAge() {
		return age;
	}

	/**
	 * (Optional)
	 * Nur positive Werte
	 * @param age
	 */
	public void setAge(int age) {
		if(age >= 0)
			this.age = age;
		else 
			System.out.println("FEHLER: Es wurde versucht, das Alter auf einen negativen Wert zu setzen");
	}

	public int getId() {
		return id;
	}

	/**
	 * Nur positive Werte
	 * @param id
	 */
	public void setId(int id) {
		if(id >= 0)
			this.id = id;
		else 
			System.out.println("FEHLER: Es wurde versucht, die ID auf einen negativen Wert zu setzen");
	}	
	
	@Override
	public String toString() {
		return String.format("ID: %d\nVorname: %s\nNachname: %s\nAlter: %d\n", this.getId(), this.getFirst(), this.getLast(), this.getAge());
	}
}