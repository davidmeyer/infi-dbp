# README #

Repository für den INFI-DBP (Informationssysteme Datenbankprogrammierung) Unterricht. Größtenteils PostreSQL Datenbankzugriffe via Java. Enthält mehrere Projekte mit jeweils eigenen Datenbanken.

### SETUP ###

* Datenbankdatei vom jeweiligen Projekt beziehen (.sql Datei)
* Lokal auf PostgreSQL installieren
* Java Code downloaden
* PostgreSQL Credentials im Java Code dem jeweiligen lokalen Setup anpassen
* (Test Daten einfügen, falls nötig)

### CONTACT ###

* E-Mail: david.meyer@students.htlinn.ac.at
* Twitter: @dave_meyer_ (https://twitter.com/Dave_Meyer_)